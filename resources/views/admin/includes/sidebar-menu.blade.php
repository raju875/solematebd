<div class="navbar nav_title" style="border: 0;">
    <a href="{{ url('/my-ecommerce-administration/dashboard') }}" class="site_title"><i class="fa fa-paw"></i> <span>SoleMate</span></a>
</div>

<div class="clearfix"></div>

<!-- menu profile quick info -->
<div class="profile clearfix">
    <div class="profile_pic">
        <img src="{{ asset('admin/profileImages') }}/{{ Auth::user()->avatar }}" class="img-circle profile_img" alt="  {{ Auth::user()->name }}">

    </div>
    <div class="profile_info">
        <span>Welcome,</span>
        <h2>  {{ Auth::user()->name }}</h2>
    </div>
</div>
<!-- /menu profile quick info -->

<br />
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <ul class="nav side-menu">
            <li><a href="{{ url('/my-ecommerce-administration/dashboard') }}"><i class="fa fa-home"></i>Home</a></li>
        </ul>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-home"></i> Mesaage <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ url('/my-ecommerce-administration/message/message-form') }}">Add New Message</a></li>
                    <li><a href="{{ url('/my-ecommerce-administration/message/message-table') }}">Message Manage</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-home"></i> Logo <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ url('/my-ecommerce-administration/logo/logo-form') }}">Logo Add</a></li>
                    <li><a href="{{ url('/my-ecommerce-administration/logo/logo-table') }}">Logo Manage</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-home"></i> Banner <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ url('/my-ecommerce-administration/banner/banner-form') }}">Add New Banner</a></li>
                    <li><a href="{{ url('/my-ecommerce-administration/banner/banner-table') }}">Manage Banner</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-home"></i> Advertisement <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ url('/my-ecommerce-administration/ad/ad-form') }}">New Ad</a></li>
                    <li><a href="{{ url('/my-ecommerce-administration/ad/ad-table') }}">Manage Ads</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-home"></i> Category <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ url('/my-ecommerce-administration/category/add-category-form') }}">Add New Category</a></li>
                    <li><a href="{{ url('/my-ecommerce-administration/category/category-table') }}">Category Table</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-home"></i> Sub Category <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ url('/my-ecommerce-administration/sub-category/sub-category-form') }}">Add New Sub Category</a></li>
                    <li><a href="{{ url('/my-ecommerce-administration/sub-category/sub-category-table') }}">Sub Category Table</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-home"></i> Brand <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ url('/my-ecommerce-administration/brand/brand-form') }}">Brand Form</a></li>
                    <li><a href="{{ url('/my-ecommerce-administration/brand/brand-table') }}">Manage Brand</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-home"></i> Color <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ url('/my-ecommerce-administration/color/color-form') }}">Color Form</a></li>
                    <li><a href="{{ url('/my-ecommerce-administration/color/color-table') }}">Manage Color</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-home"></i> Product <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ url('/my-ecommerce-administration/product/product-form') }}">Product Form</a></li>
                    <li><a href="{{ url('/my-ecommerce-administration/product/product-table') }}">Product Table</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-home"></i> Order <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ url('/my-ecommerce-administration/order/pending-order') }}">Pending Order</a></li>
                    <li><a href="{{ url('/my-ecommerce-administration/order/successful-delivery') }}">Successful Delivery</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav side-menu">
            <li>
                <a href="{{ url('/my-ecommerce-administration/stock/stock-report') }}"><i class="fa fa-home"></i>Stock</a>
            </li>
        </ul>
    </div>
</div>