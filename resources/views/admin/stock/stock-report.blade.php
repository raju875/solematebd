@extends('admin.master')

@section('title')
    <title>Stock | Solemate</title>
    @endsection

@section('content')
    <div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Stocks</h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text"  id="js-search" class="form-control" placeholder="Search for...">
                    </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Update Data</h2>
                        @if(Session::has('message'))
                            <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                        @endif
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <br>
                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action js-table">
                                <thead>
                                <tr class="headings">
                                    <th class="column-title">SL </th>
                                    <th class="column-title">Category </th>
                                    <th class="column-title">Product </th>
                                    <th class="column-title">Model No </th>
                                    <th class="column-title">Color </th>
                                    <th class="column-title">Size </th>
                                    <th class="column-title">Quantity </th>
                                    <th class="column-title">Price </th>
                                    <th class="column-title no-link last"><span class="nobr">BDT</span>
                                    </th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php $i=1?>
                                <?php $quantity=0?>
                                <?php $grandTotal=0 ?>
                                @foreach($products as $product )
                                <tr class="even pointer">
                                    <td class=" ">{{ $i }}</td>
                                    @foreach($subCategories as $subCategory)
                                        @if($product->sub_category_id == $subCategory->id )
                                            @foreach($categories as $category )
                                                @if($subCategory->category_id==$category->id)
                                                    <td class=" ">{{ $category->category_name }}</td>
                                                @endif
                                            @endforeach
                                    <td class=" ">{{ $subCategory->sub_category_name }}</td>
                                        @endif
                                    @endforeach
                                    <td>{{ $product->code_no }}</td>
                                    <?php $color = \App\Color::find($product->product_color)?>
                                    <td>{{ $color->color }}</td>
                                    <td>{{ $product->product_size }}</td>
                                    <td>
                                        <?php $qty = $product->product_quantity?>
                                        <form action="{{ url('/my-ecommerce-administration/stock/update-stock') }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <input type="number" min="0" name="product_quantity" value="{{ $qty }}">
                                            <input type="submit" name="btn" value="update">
                                        </form>
                                    </td>
                                    <td>{{ $product->product_prize }}</td>
                                    <td>{{ $total = $product->product_quantity*$product->product_prize }}/=</td>
                                    </td>
                                </tr>
                                    <?php $i++?>
                                    <?php $quantity   = $quantity + $qty ?>
                                    <?php $grandTotal = $grandTotal + $total ?>
                                    @endforeach
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td><h3>Total</h3></td>
                                    <td></td>
                                    <td><h3>=</h3></td>
                                    <td></td>
                                    <td><h3>{{ $quantity }}</h3><h5>pcs</h5></td>
                                    <td></td>
                                    <td><h3>{{ $grandTotal }}/=</h3></td>
                                </tr>
                             </tbody>
                            </table>
                            <table class="even pointer" id="suc">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    @endsection