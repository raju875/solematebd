
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Pos Chart</title>
    <!--Style -->
    <style>
        body{
            margin: 50px 60px;
        }
        .left{
            width: 30%;
            float: left;
        }
        .right {
            width: 30%;
            float: right;
            text-align: left;
            text-align-last: end;
        }
        .center{
            text-align: center;
            width: 30%;
            float: left;
        }
        table, th, td{
            border: 1px solid black;
            border-collapse: collapse;
        }
        td{
            padding: 0 10px;
        }
        .border-none {
            border: none;
            text-align-last: end;
            padding: 10px 60px;
        }
        .text-center p {
            padding: 100px;
        }
        .bottom {
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            padding: 0 60px;
        }
    </style>
    <!--Style -->
</head>

<body>
<section>
    <div class="left">
        <h3>Meer Sajib</h3>
        <address>
            {{ $shipping->street_address }}, {{ $shipping->village }}, {{ $shipping->sub_district }} <br/>
            {{ $shipping->district }}, {{ $shipping->zip }}
        </address>
        <p>{{ $shipping->mobile_no }}</p>
    </div>
    <div class="center">
        @foreach($logos as $logo )
            <img src="{{ asset($logo->logo) }}" height="60px" width="140px"/>
        @endforeach
    </div>
    <div class="right">
        <h3>Invoice: #{{ $order->id }}</h3>
        <h3>Date: {{ $order->created_at->format('d/m/Y')  }}</h3>
        <h3>Time: {{ $order->created_at->format('H:i:s')  }}</h3>
        <?php $grandTotal = 0 ?>
        @foreach($details as $detail )
            <?php $detail->product_price ?>
            <?php $detail->product_quantity ?>
            <?php $total = $detail->product_quantity*$detail->product_price ?>
            <?php $grandTotal = $grandTotal + $total ?>
        @endforeach
        <h3>Ammount: {{ $grandTotal }} /-</h3>

    </div>
</section>
<section>
    <table style="width:100%">
        <tr>
            <th>SL</th>
            <th>Item</th>
            <th>Model</th>
            <th>Color</th>
            <th>Size</th>
            <th>Unit Price</th>
            <th>QTY</th>
            <th>Price</th>
        </tr>
        <?php $i=1?>
        <?php $grandTotal=0?>
        @foreach($details as $detail )
            <tr>
                <td>{{ $i }}</td>
                <td>{{ $detail->product_name }}</td>
                <td>{{ $detail->product_model }}</td>
                <td>{{ $detail->product_color }}</td>
                <td>{{ $detail->product_size }}</td>
                <td>{{ $detail->product_price }}</td>
                <td>{{ $detail->product_quantity }}</td>
                <td>{{ $total = $detail->product_quantity*$detail->product_price }}</td>
                <?php $grandTotal = $grandTotal + $total ?>
            </tr>
        @endforeach
        <tfoot>
        <tr>
            <td class="border-none" colspan="7">Total: {{ $grandTotal }} /-</td>
        </tr>
        </tfoot>
    </table>
</section>
<section class="bottom">
    <div class="center" style="width: 100%;border-bottom: 1px solid">
        <h5>Terms & Conditions</h5>
    </div>
    <div class="text-center">
        <p>Unloaded goods are not refunded !</p>
    </div>
</section>
</body>

</html>
