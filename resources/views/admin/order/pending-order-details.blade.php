@extends('admin.master')

@section('title')
    <title>Order Details | Solemate</title>
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Pending <small>Order Details</small></h3>
                    @if(Session::has('message'))
                        <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                    @endif
                    @if(Session::has('alert'))
                        <h3 class="text text-center text-danger">{{ Session::get('alert') }}</h3>
                    @endif
                    <br>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="clearfix"></div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <h3>Customer Info For This Order</h3>
                            <table class="table table-bordered">
                                <?php $i=1 ?>
                                <tr>
                                    <td>Customer Name</td>
                                    <td>{{ $customer->name }}</td>
                                </tr>
                                <tr>
                                    <td>Customer Email</td>
                                    <td>{{ $customer->email }}</td>
                                </tr>
                                <tr>
                                    <td>Mobile No</td>
                                    <td>{{ $customer->mobile_no }}</td>
                                </tr>
                            </table>
                        </div>
                        <hr/>
                        <div class="x_content">
                            <h3>Shipping Info For This Order</h3>
                            <table class="table table-bordered">
                                <tr>
                                    <td>Customer Name</td>
                                    <td>{{ $shipping->first_name }} {{ $shipping->last_name }}</td>
                                </tr>
                                <tr>
                                    <td>Customer Email</td>
                                    <td>{{ $shipping->email }}</td>
                                </tr>
                                <tr>
                                    <td>Street Address</td>
                                    <td>{{ $shipping->street_address }}</td>
                                </tr>
                                <tr>
                                    <td>Village</td>
                                    <td>{{ $shipping->village }}</td>
                                </tr>
                                <tr>
                                    <td>District</td>
                                    <td>{{ $shipping->district }}</td>
                                </tr>
                                <tr>
                                    <td>Sub District</td>
                                    <td>{{ $shipping->sub_district }}</td>
                                </tr>
                                <tr>
                                    <td>Zip</td>
                                    <td>{{ $shipping->zip }}</td>
                                </tr>
                                <tr>
                                    <td>Mobile No</td>
                                    <td>{{ $shipping->mobile_no }}</td>
                                </tr>
                            </table>
                        </div>
                        <hr/>

                        <h3>Order Details</h3>
                        <div class="x_content">

                            <table class="table table-bordered">
                                <thead>
                                <?php $i=1 ?>
                                <tr>
                                    <th>Sl No</th>
                                    <th>Product Name</th>
                                    <th>Brand Name</th>
                                    <th>Product Model</th>
                                    <th>Product Color</th>
                                    <th>Product Size</th>
                                    <th>Product Quantity</th>
                                    <th>Product Price</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $total=0 ?>
                                <?php $grandTotal=0 ?>
                                @foreach($details as $detail )
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ $detail->product_name }}</td>
                                        <td>{{ $detail->brand_name }}</td>
                                        <td>{{ $detail->product_model }}</td>
                                        <?php $color = \App\Color::find($detail->product_color)?>
                                        <td>{{ $color->color }}</td>
                                        <td>{{ $detail->product_size }}</td>
                                        <td>{{ $detail->product_quantity }}</td>
                                        <td>{{ $detail->product_price }}</td>
                                        <?php $total = $detail->product_quantity*$detail->product_price?>
                                        <td>{{ $total }}</td>
                                        <?php $grandTotal = $grandTotal+$total?>
                                    </tr>
                                    <?php $i++ ?>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        </div>
                    <h2 class="text-center">Grand Total : {{ $grandTotal }}/=</h2>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection