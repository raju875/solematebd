@extends('admin.master')

@section('title')
    <title>Order | Solemate</title>
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Order <small>Table</small></h3>
                    @if(Session::has('message'))
                        <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                    @endif
                    @if(Session::has('alert'))
                        <h3 class="text text-center text-danger">{{ Session::get('alert') }}</h3>
                    @endif
                    <br>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="clearfix"></div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage<small>Oeder Info</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <table class="table table-bordered">
                                <thead>
                                <?php $i=1 ?>
                                <tr>
                                    <th>Sl No</th>
                                    <th>Customer Name</th>
                                    <th>Order Total</th>
                                    <th>Order Status</th>
                                    <th>Orde Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order )
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ $order->name }}</td>
                                        <td>{{ $order->order_total }}</td>
                                        <td>{{ $order->order_status }}</td>
                                        <td>{{ $order->created_at }}</td>
                                        <td>

                                            <a href="{{  url('/my-ecommerce-administration/order/deliver-details/'.$order->id) }}" class="btn btn-info btn-xs" title="deliver-details">
                                                <span class="glyphicon glyphicon-zoom-in"></span>
                                            </a>
                                            {{--<a href="{{  url('/my-ecommerce-administration/invoice/invoice-view/'.$order->id) }}" class="btn btn-success btn-xs" title="view-invoice">--}}
                                                {{--<span class="glyphicon glyphicon-zoom-out"></span>--}}
                                            {{--</a>--}}
                                            {{--<a href="{{  url('/my-ecommerce-administration/invoice/pdf/'.$order->id) }}" class="btn btn-primary btn-xs" title="view-invoice">--}}
                                                {{--<span class="glyphicon glyphicon-download"></span>--}}
                                            {{--</a>--}}
                                            {{--<a href="{{  url('/my-ecommerce-administration/order/confirm-order/'.$order->id) }}" onclick="return confirm('Are you sure to delete it !!!')" class="btn btn-success btn-xs" title="delete">--}}
                                                {{--<span class="glyphicon glyphicon-ok"></span>--}}
                                            {{--</a>--}}
                                            <a href="{{  url('/my-ecommerce-administration/order/delete-delivery-info/'.$order->id) }}" onclick="return confirm('Are you sure to remove it !!!')" class="btn btn-primary btn-xs" title="delete">
                                                <span class="glyphicon glyphicon-remove"></span>
                                            </a>
                                            {{--<a href="{{  url('/my-ecommerce-administration/category/view-category-info/'.$banner->id) }}" class="btn btn-primary btn-xs" title="view">--}}
                                            {{--<span class="glyphicon glyphicon-search"></span>--}}
                                            {{--</a>--}}
                                        </td>

                                    </tr>
                                    <?php $i++ ?>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $orders->links() }}
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection