@extends('admin.master')

@section('title')
    <title>Solemate | Product</title>
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Product <small>Table</small></h3>
                    @if(Session::has('message'))
                        <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                    @endif
                    @if(Session::has('alert'))
                        <h3 class="text text-center text-danger">{{ Session::get('alert') }}</h3>
                    @endif
                    <br>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="clearfix"></div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage<small>Product Info</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <table class="table table-bordered">
                                <thead>
                                <?php $i=1 ?>
                                <tr>
                                    <th>Sl No</th>
                                    <th>Product Category</th>
                                    <th>Product Sub Category</th>
                                    <th>Brand Name</th>
                                    <th>Code No</th>
                                    <th>Product Main Image</th>
                                    <th>Sub Images</th>
                                    <th>Product Size</th>
                                    <th>Product Quantity</th>
                                    <th>Prize (BDT)</th>
                                    <th>Color</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product )
                                    <tr>
                                        <td>{{ $i }}</td>
                                        @foreach($categories as $category )
                                            @if($product->sub_category_id == $category->id )
                                        <td>{{ $category->category_name }}</td>
                                            @endif
                                        @endforeach
                                        <td>{{ $product->sub_category_name }}</td>
                                        <td>{{ $product->brand_name }}</td>
                                        <td><img src="{{ asset($product->image ) }}" width="100" height="70"></td>
                                        <td>
                                            @foreach($subImages as $subImage )
                                                @if($subImage->product_id == $product->id )
                                               <a href="{{ url('/my-ecommerce-administration/product/delete-product-sub-image/'.$subImage->id) }}"  onclick="return confirm('Are you sure to delete it !!!')">
                                                   <img src="{{ asset($subImage->sub_image ) }}" width="100" height="70">
                                               </a>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{ $product->code_no }}</td>
                                        <td>{{ $product->product_size }}</td>
                                        <td>{{ $product->product_quantity }}</td>
                                        <td>{{ $product->product_prize }} </td>
                                        <td>{{ $product->product_color }}</td>
                                         <td>
                                             {{ substr(strip_tags($product->description), 0, 100) }}
                                            <a href="{{  url('/manan-administration2018/blog/view-blog/'.$product->id) }}"><?php echo $product->description ?></a>
                                        </td>
                                        <td>
                                            @if($product->status == 1 )
                                                <a href="{{ url('/my-ecommerce-administration/product/unpublished-product/'.$product->id) }}" class="btn btn-success btn-xs" title="published">
                                                    <span class="glyphicon glyphicon-arrow-up"></span>
                                                </a>
                                            @else
                                                <a href="{{ url('/my-ecommerce-administration/product/published-product/'.$product->id) }}" class="btn btn-warning btn-xs" title="unpublished">
                                                    <span class="glyphicon glyphicon-arrow-down"></span>
                                                </a>
                                            @endif

                                            <a href="{{  url('/my-ecommerce-administration/product/editable-product-form/'.$product->id) }}" class="btn btn-primary btn-xs" title="edit">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </a>
                                            <a href="{{  url('/my-ecommerce-administration/product/delete-product-info/'.$product->id) }}" onclick="return confirm('Are you sure to delete it !!!')" class="btn btn-primary btn-xs" title="delete">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                            {{--<a href="{{  url('/my-ecommerce-administration/category/view-category-info/'.$banner->id) }}" class="btn btn-primary btn-xs" title="view">--}}
                                            {{--<span class="glyphicon glyphicon-search"></span>--}}
                                            {{--</a>--}}
                                        </td>

                                    </tr>
                                    <?php $i++ ?>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection