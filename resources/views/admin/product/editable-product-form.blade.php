@extends('admin.master')

@section('title')
    <title>Solemate | Product</title>
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3 class="align-content-center">Product Form</h3>
                    @if(Session::has('message'))
                        <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                    @endif
                    @if(Session::has('aleart'))
                        <h3 class="text text-center text-danger">{{ Session::get('alert') }}</h3>
                    @endif
                    <br>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add New Product</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />
                            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ url('/my-ecommerce-administration/product/update-product') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="hidden" name="product_id" required="required" value="{{ $productById->id }}" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Product Category <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select  name="category_id" id="category_id" required="required" class="form-control col-md-8 col-xs-12">
                                            @foreach($categories as $category )
                                                <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                            @endforeach
                                        </select>
                                        <span style="color: red">{{ $errors->has('status') ? $errors->first('status') : ' ' }}</span>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Product Sub Category <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select  name="sub_category_id" id="sub_category_id" class="form-control col-md-8 col-xs-12">
                                            <option value="">Select Sub Category</option>
                                        </select>
                                        <span style="color: red">{{ $errors->has('sub_category_id') ? $errors->first('sub_category_id') : ' ' }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Product Brands <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select  name="brand_id" required="required" class="form-control col-md-8 col-xs-12">
                                            @foreach($brands as $brand )
                                                <option value="{{ $brand->id }}">{{ $brand->brand_name }}</option>
                                            @endforeach
                                        </select>
                                        <span style="color: red">{{ $errors->has('status') ? $errors->first('status') : ' ' }}</span>

                                    </div>
                                </div>
                                <?php $productColor = \App\Color::find($productById->product_color)?>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Product Color <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select  name="product_color" required="required" class="form-control col-md-8 col-xs-12">
                                            <option value="{{ $productColor->id }}">{{ $productColor->color }}</option>
                                            <option value="">Select Color</option>
                                            <?php $colors = App\Color::where('status',1)->get()?>
                                            @if(!empty($colors))
                                            @foreach($colors as $color )
                                                @if($color->id != $productColor->id )
                                                <option value="{{ $color->id }}">{{ $color->color }}</option>
                                                @endif
                                            @endforeach
                                                @endif
                                        </select>
                                        <span style="color: red">{{ $errors->has('product_color') ? $errors->first('product_color') : ' ' }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Product Main Image</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" name="image" class="form-control" onchange="previewImage(event)" >
                                        <img src="{{ asset($productById->image ) }}" width="100" height="70" id="image-field">
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Product Sub Images</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" name="sub_image[]" class="form-control" multiple>
                                        @foreach($subImages as $subImage )
                                        <img src="{{ asset($subImage->sub_image ) }}" width="100" height="70">
                                        @endforeach
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Product Code No <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="code_no" required="required" value="{{ $productById->code_no }}" class="form-control col-md-7 col-xs-12">
                                        <span style="color: red">{{ $errors->has('code_no') ? $errors->first('code_no') : ' ' }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Product Size <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="product_size" required="required" value="{{ $productById->product_size }}" class="form-control col-md-7 col-xs-12">
                                        <span style="color: red">{{ $errors->has('product_size') ? $errors->first('product_size') : ' ' }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Product Quantuty <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="product_quantity" required="required" value="{{ $productById->product_quantity }}" class="form-control col-md-7 col-xs-12">
                                        <span style="color: red">{{ $errors->has('product_quantity') ? $errors->first('product_quantity') : ' ' }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Product Prize <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="product_prize" required="required" value="{{ $productById->product_prize }}"  class="form-control col-md-7 col-xs-12">
                                        <span style="color: red">{{ $errors->has('product_prize') ? $errors->first('product_prize') : ' ' }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Product Description<span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                       <textarea  name="description" id="description" class="form-control" rows="10" cols="8" required >
                                          {{ $productById->description }}
                                       </textarea>
                                        <span style="color: red">{{ $errors->has('description') ? $errors->first('description') : ' ' }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Status <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select  name="status" required="required" class="form-control col-md-8 col-xs-12">
                                            @if($productById->status==1 )
                                            <option value="1">Published</option>
                                            <option value="0">Unpublished</option>
                                                @else
                                                <option value="0">Unpublished</option>
                                                <option value="1">Published</option>
                                                @endif
                                        </select>
                                        <span style="color: red">{{ $errors->has('status') ? $errors->first('status') : ' ' }}</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Size Guide</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="file" name="size_guide" class="form-control" onchange="previewSizeGuide(event)" >
                                                <img src="{{ asset($productById->size_guide ) }}" width="100" height="70" id="image-field3">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button class="btn btn-primary" type="button">Cancel</button>
                                        <button class="btn btn-primary" type="submit" value="copy" name="btn">Copy</button>
                                        <button type="submit" class="btn btn-success" value="update" name="btn">Update</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end form for validations -->
    {{--dropdown select start--}}
    <script>
        var categoryId = document.getElementById('category_id');
        categoryId.onblur = function() {
            var catId   = document.getElementById('category_id').value;
            var xmlHttp = new XMLHttpRequest();
            var serverPage = 'http://localhost:8080/sajib-ecommerce/public/my-ecommerce-administration/ajax-dynamic-dependency-dropdown/'+catId;
            xmlHttp.open('GET',serverPage);
            xmlHttp.onreadystatechange = function () {
                if (xmlHttp.readyState==4 && xmlHttp.status==200 ) {
                    document.getElementById('sub_category_id').innerHTML = xmlHttp.responseText;
                }
            }
            xmlHttp.send(null);
        }
    </script>
    {{--dropdown select end--}}

    {{--preview before upload start--}}
    <script type="text/javascript">
        function previewImage(event) {
            var reader = new FileReader();
            var imageField = document.getElementById("image-field");
            reader.onload = function () {
                if (reader.readyState==2 ) {
                    imageField.src = reader.result;
                }
            }
            reader.readAsDataURL(event.target.files[0]);
        }

        function previewSizeGuide(event) {
            var reader = new FileReader();
            var imageField = document.getElementById("image-field3");
            reader.onload = function () {
                if (reader.readyState==2 ) {
                    imageField.src = reader.result;
                }
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>
    {{--preview brfore upload end--}}
@endsection
