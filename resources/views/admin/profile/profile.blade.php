{{--@extends('admin.master')--}}

{{--@section('title')--}}
{{--<title> Solemate</title>--}}
{{--@endsection--}}

{{--@section('content')--}}
{{--<div class="row">--}}

{{--<h2>{{$user->name}}</h2>--}}
{{--</div>--}}
{{--@endsection--}}

<h2>{{ $user->name }}</h2>
<img src="{{ asset('/') }}/admin/profileImages/{{ $user->avatar }}" style="width:150px; height: 120px; border-radius:50%">

<h1>Update Profile Picture</h1>
<form method="post" action="{{ url('/my-ecommerce-administration/profile/update-profile') }}" enctype="multipart/form-data">
    @csrf
    <input type="file" name="avatar" required>
    <input type="submit" name="btn">
</form>