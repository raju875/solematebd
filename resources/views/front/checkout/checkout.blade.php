@extends('front.master')

@section('title')
    Checkout ! SolemateBd
    @endsection

@section('content')
    <!--checkout form starts-->
    <section id="checkout-form">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">

                            <!--Grid row-->
                            <div class="row">

                                <!--Grid column-->
                                <div class="col p-5">
                                    @if(Session::has('message'))
                                        <p class="text-muted text-center">  {{ Session::get('message') }} </p>
                                @endif
                                        @if(Session::has('alert'))
                                            <p class="text-danger text-center">  {{ Session::get('alert') }} </p>
                                    @endif
                                    <!-- Pills navs -->
                                    <div class="nav-justified font-weight-bold text-center pb-5">
                                        Login
                                    </div>

                                    <form @if(Session::get('grandTotal')==0) action="{{ url('/user-login-check') }}" @else action="{{ url('/login-to-order') }}" @endif method="POST">
                                        @csrf
                                         <!--email-->
                                        <label for="email" class="">Email *</label>
                                        <input type="email" name="email" id="email" class="form-control mb-4" placeholder="youremail@example.com" onblur="emailCheck(this.value)" required>
                                        <span style="color: red">{{ $errors->has('email') ? $errors->first('email') : ' ' }}</span>

                                         <!--password no-->
                                         <label for="password" class="">Password *</label>
                                        <input type="password" name="password" id="password" class="form-control mb-4" placeholder="youremail@example.com" required>
                                        <span style="color: red">{{ $errors->has('email') ? $errors->first('email') : ' ' }}</span>

                                        <hr>
                                        <button class="btn btn-danger btn-lg btn-block" type="submit">Login</button>
                                    </form>
                                </div>
                                <!--Grid column-->
                            </div>
                            <!--Grid row-->

                            <div class="row">

                                <!--Grid column-->
                                <div class="col p-5">

                                    <!-- Pills navs -->
                                    <div class="nav-justified font-weight-bold text-center pb-5">
                                        Register
                                    </div>

                                    <form @if(Session::get('grandTotal')==0) action="{{ url('/user-register') }}" @else action="{{ url('/register-to-order') }}" @endif method="POST">
                                    @csrf
                                    <!--Username-->
                                        <label for="email" class="">Name *</label>
                                        <input type="text" name="name" id="name" class="form-control mb-4" placeholder="youremail@example.com" required>
                                        <span style="color: red">{{ $errors->has('name') ? $errors->first('name') : ' ' }}</span>

                                        <!--email-->
                                        <label for="email" class="">Email *</label>
                                        <input type="email" name="email" id="email" class="form-control mb-4" placeholder="youremail@example.com" required>
                                        <span style="color: red">{{ $errors->has('email') ? $errors->first('email') : ' ' }}</span>

                                        <!--mobile no-->
                                        <label  class="">Mobile No *</label>
                                        <input type="number" name="mobile_no"  class="form-control mb-4"  required>
                                        <span style="color: red">{{ $errors->has('mobile_no') ? $errors->first('mobile_no') : ' ' }}</span>

                                        <!--mobile no-->
                                        <label for="password" class="">Password *</label>
                                        <input type="password" name="password" id="password" class="form-control mb-4" placeholder="youremail@example.com" required>
                                        <span style="color: red">{{ $errors->has('password') ? $errors->first('password') : ' ' }}</span>

                                        <hr>
                                        <button class="btn btn-danger btn-lg btn-block" type="submit">Register</button>
                                    </form>
                                </div>
                                <!--Grid column-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--checkout form ends-->

    <script>
        function emailCheck(email) {
            alert('trst')
            $.ajax({
                url : 'http://localhost:8080/sajib-ecommerce/public/email-check/'.email,
                method : 'GET',
                data   : {email:email},
                dataType : 'JSON',
                sussess : function (data) {
                    alert (data);
                }

            });
        }
    </script>

    @endsection