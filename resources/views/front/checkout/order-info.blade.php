@extends('front.master')

@section('title')
    Order Info ! SolemateBd
@endsection

@section('content')
    <!--checkout form starts-->
    <section id="checkout-form">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">

                            <!--Grid row-->
                            <div class="row">

                                <!--Grid column-->
                                <div class="col p-5">

                                    <!-- Pills navs -->
                                    <div class="nav-justified font-weight-bold text-center pb-5">
                                        Shippinh Info
                                    </div>
                                    @if(Session::has('message'))
                                        <p class="text-muted text-center">  {{ Session::get('message') }} </p>
                                    @endif

                                    <form action="{{ url('/order-confirm') }}" method="POST">
                                    @csrf
                                    <!--Grid row-->
                                        <div class="row">

                                            <!--Grid column-->
                                            <div class="col-md-6 mb-4">

                                                <!--firstName-->
                                                <label for="firstName" class="">First name *</label>
                                                <input type="text" id="firstName" name="first_name" class="form-control" required>
                                                <span style="color: red">{{ $errors->has('first_name') ? $errors->first('first_name') : ' ' }}</span>
                                            </div>
                                            <!--Grid column-->

                                            <!--Grid column-->
                                            <div class="col-md-6 mb-2">

                                                <!--lastName-->
                                                <label for="lastName" class="">Last name *</label>
                                                <input type="text" id="lastName" name="last_name" class="form-control" required>
                                                <span style="color: red">{{ $errors->has('last_name') ? $errors->first('last_name') : ' ' }}</span>
                                            </div>
                                            <!--Grid column-->
                                        </div>
                                        <!--Grid row-->

                                        <!--Username-->
                                        <!--email-->
                                        <label for="email" class="">Email *</label>
                                        <input type="text" name="email" id="email" class="form-control mb-4" placeholder="youremail@example.com" required>
                                        <span style="color: red">{{ $errors->has('email') ? $errors->first('email') : ' ' }}</span>

                                        <!--address-->
                                        <label for="address" class="">Street Address</label>
                                        <input type="text" name="street_address" id="address" class="form-control mb-4" placeholder="1234 Main St" required >
                                        <span style="color: red">{{ $errors->has('street_address') ? $errors->first('street_address') : ' ' }}</span>

                                        <!--address-2-->
                                        <label for="address-2" class="">Village / Town</label>
                                        <input type="text" name="village" id="address-2" class="form-control mb-4" placeholder="Apartment or suite" required >
                                        <span style="color: red">{{ $errors->has('village') ? $errors->first('village') : ' ' }}</span>

                                        <!--Grid row-->
                                        <div class="row">

                                            <!--Grid column-->
                                            <div class="col-lg-4 col-md-6 mb-4">
                                                <label for="zip">District</label>
                                                <input type="text" class="form-control" name="district" placeholder="" required>
                                                <span style="color: red">{{ $errors->has('district') ? $errors->first('district') : ' ' }}</span>
                                            </div>
                                            <!--Grid column-->

                                            <!--Grid column-->
                                            <div class="col-lg-4 col-md-6 mb-4">

                                                <label for="zip">Sub District</label>
                                                <input type="text" class="form-control" name="sub_district" placeholder="" required>
                                                <span style="color: red">{{ $errors->has('sub_district') ? $errors->first('sub_district') : ' ' }}</span>

                                            </div>
                                            <!--Grid column-->

                                            <!--Grid column-->
                                            <div class="col-lg-4 col-md-6 mb-4">

                                                <label for="zip">Zip</label>
                                                <input type="text" class="form-control" name="zip" id="zip" placeholder="" required>
                                                <span style="color: red">{{ $errors->has('zip') ? $errors->first('zip') : ' ' }}</span>

                                            </div>
                                            <div class="col">
                                                <label for="mobile">Mobile No</label>
                                                <input type="text" name="mobile_no" class="form-control" id="mobile" placeholder="+880" required>
                                                <span style="color: red">{{ $errors->has('mobile_no') ? $errors->first('mobile_no') : ' ' }}</span>

                                            </div>
                                            <!--Grid column-->

                                        </div>
                                        <!--Grid row-->

                                        <hr>
                                        <button class="btn btn-danger btn-lg btn-block" type="submit">Confirm Order</button>
                                    </form>
                                </div>
                                <!--Grid column-->
                            </div>
                            <!--Grid row-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--checkout form ends-->
@endsection