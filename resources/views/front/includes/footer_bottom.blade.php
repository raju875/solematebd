<!--Footer Bottom here-->
<footer id="footer-bottom">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <p class="text-left float-left"><i class="far fa-copyright"></i>SolemateBD 2018</p>
            </div>
            <div class="col">
                <p class="text-right float-right">Developed by: Al-Amin-Raju & Meer Sajib</p>
            </div>
        </div>
    </div>
</footer>
<!--Footer Bottom Ends here-->
