<!--Navbar Starts from here-->
<div class="container-fluid no-padding top-nav">
    <div class="row">
        <div class="col padding">
            <div class="text-left">
                <?php $messages = DB::table('messages')->where('status',1)->get()?>
                @foreach($messages as $message )
                <a class="top-nav-link text-left ml-5" href="#"><span class="hot-msg">Hot</span><?php $message->message?></a>
                    @endforeach
            </div>
        </div>
        <div class="col">
            <div class="text-right padding">
                @if(Session::get('customerId'))
                    <li>
                        <a class="top-nav-link text-right" href="{{ url('/user-logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ url('/user-logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                @else
                    <a class="top-nav-link text-right" href="{{ url('/user-login') }}">Login</a>
                    <a class="top-nav-link text-right" href="{{ url('/user-login') }}">Register</a>
                @endif
                <a class="top-nav-link text-right" href="#"><i class="fas fa-heart"></i></a>
                <a class="top-nav-link text-right" href="#"><i class="fas fa-shopping-cart"></i></a>
            </div>
        </div>
    </div>
</div>
</div>
<div class="clr"></div>
<!--Navbar Ends here-->