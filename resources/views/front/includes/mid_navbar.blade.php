<section id="search">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-sm-12 col-md-3">
                <?php $logos = DB::table('logos')->where('status',1)->get();?>
                @foreach($logos as $logo )
                <img class="float-left img-fluid" src="{{ asset($logo->logo) }}" height="1120" width="180"/>
                    @endforeach
            </div>
            <div class="col-sm-12 col-md-6">
                <form action="">
                    <div class="input-group md-form form-sm form-2 pl-0 search-from">
                        <input class="form-control my-0 py-1 red-border" type="text" placeholder="Search" aria-label="Search">
                        <div class="input-group-append">
                            <span class="input-group-text search-btn lighten-3" id="basic-text1">Search</span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-12 col-md-3">
                <ul class="nav justify-content-end search-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/show-cart') }}"><i class="fas fa-3x fa-shopping-cart"></i><span class="cart-list">{{ Session::get('totalItems') }}</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>