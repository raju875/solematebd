<!--Navbar-->
<section id="nav-bottom" class=" sticky-top">
    <div class="container-fluid">
        <div class="row">
            <nav class="navbar navbar-expand-lg navbar-dark special-color-dark nav-bottom">

                <!-- Navbar brand -->
                <a class="navbar-brand text-uppercase" href="{{ url('/') }}">SolemateBD</a>

                <!-- Collapse button -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1"
                        aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

                <!-- Collapsible content -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent1">
                <?php $categories = DB::table('categories')->where('status',1)->get()?>
                    <!-- Links -->
                    <ul class="navbar-nav mx-auto">
                        @foreach($categories as $category )
                        <li class="nav-item dropdown mega-dropdown">
                            <a class="nav-link dropdown-toggle text-uppercase no-caret"  data-hover="dropdown"  data-toggle="dropdown" data-close-others="false" aria-haspopup="true" aria-expanded="false">{{ $category->category_name }}</a>
                                <div class="dropdown-menu mega-menu v-2 row z-depth-1 special-color" aria-labelledby="navbarDropdownMenuLink1">
                                <div class="row mx-md-4 mx-1">
                                        <?php $subCategories = DB::table('sub_Categories')->where('category_id',$category->id )->get()?>
                                    @foreach($subCategories->chunk(4) as $subCategories )
                                    @foreach($subCategories as $subCategory )
                                    <div class="col-md-6 col-xl-3 sub-menu my-xl-5 mt-4 mb-4">
                                        <h6 class="sub-title text-uppercase font-weight-bold white-text">{{ $subCategory->sub_category_name }}</h6>
                                        <!--Featured image-->
                                        <div class="view overlay mb-3 z-depth-1">
                                            <?php $products = DB::table('products')->where('sub_category_id',$subCategory->id )->orderBy('id','desc')->get() ?>
                                            @foreach($products->chunk(4) as $products )
                                            @foreach($products as $product )
                                            <ul>
                                                <li class="float-left"><a href="{{ url('/product-page/'.$product->id) }}"><img src="{{ asset($product->image) }}" width="150" height="80" class="mega-menu-img" alt="First sample image"></a></li>
                                                <li class="float-left">
                                                    <ul class="megamenu-price">
                                                        <li><h4>ML {{ $product->code_no }}</h4></li>
                                                        <li><h4>{{ $product->product_prize }}Taka</h4></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                                @endforeach
                                                @endforeach
                                        </div>
                                    </div>

                                        @endforeach
                                        @endforeach
                                </div>
                            </div>
                        </li>
                            @endforeach
                    </ul>
                    <!-- Links -->
                </div>
                <!-- Collapsible content -->

            </nav>
        </div>
    </div>
</section>
<!--/.Navbar-->