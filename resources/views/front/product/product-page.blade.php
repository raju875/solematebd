@extends('front.master')

@section('title')
    Product Page | Solemate
    @endsection

@section('content')
    <!--Product Details Descritption Starts-->
    <section id="product-details padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-6" id="image-indicators">
                    <!--Carousel Wrapper-->
                    <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">
                        <!--Slides-->
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <img class="d-block product-img-select w-100"  src="{{ asset($productById->image ) }}" class="img-fluid">
                            </div>
                            @foreach($subImages as $subImage )
                            <div class="carousel-item">
                                <img class="d-block w-100 product-img-select" src="{{ asset($subImage->sub_image) }}" class="img-fluid">
                            </div>
                                @endforeach
                        </div>
                        <!--/.Slides-->
                        <!--Controls-->
                        <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        <!--/.Controls-->
                        <ol class="carousel-indicators image-indicators">
                            <?php $count=0 ?>
                            <li data-target="#carousel-thumb" data-slide-to="{{$count}}" class="active"> <img class="d-block w-100" src="{{ asset($productById->image ) }}" class="img-fluid"></li>
                            @foreach($subImages as $key=>$subImage )
                                    <?php $count++ ?>
                                <li data-target="#carousel-thumb" data-slide-to="{{$count}}"><img class="d-block w-100 product-img-select" src="{{ asset($subImage->sub_image) }}" class="img-fluid"></li>
                            @endforeach
                        </ol>
                    </div>
                    <!--/.Carousel Wrapper-->
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="pdoduct-description">
                        <h2>Model : {{ $productById->code_no }}</h2>
                        <p>Category : {{ $category->category_name }}</p>
                        <p>Product : {{ $subCategory->sub_category_name }}</p>
                        <p>Brand : {{ $branName->brand_name }}</p>
                        <p>Size : {{ $productById->product_size }}</p>
                        <?php $pColor = \App\Color::find($productById->product_color)?>
                        <p>Color : {{ $pColor->color }}</p>
                        @if($productById->product_quantity != 0 )
                        <p>Availability : In Stock ({{ $productById->product_quantity }})</p>
                        @else
                            <p>Availability : {{ 'Not Available' }}</p>
                        @endif

                        <h4>{{ $productById->product_prize }} TK</h4>
                        <hr>
                        <?php $productByModel   = DB::table('products')->where('code_no',$productById->code_no)->select('product_size')->distinct()->get(); ?>
                        <div class="choose-option">
                            <ul class="size-choose">
                                @foreach($productByModel as $model )
                                <li class="sizes"><a href="{{ url('/product-page/model_'.$productById->code_no.'/size_'.$model->product_size) }}">{{ $model->product_size }}</a></li>
                                    @endforeach
                            </ul>
                            <div class="clr"></div>.
                            <?php $colors = DB::table('products')->where('code_no',$productById->code_no)->where('product_size',$productById->product_size)->join('colors','products.product_color','=','colors.id')->select('products.id','colors.color')->get();?>
                            <ul class="color-choose">
                                @foreach($colors as $color )
                                    @if($color->color == 'blue' )
                                        <li class="colors"><a href="{{ url('/product-page/'.$color->id) }}"><div class="color-blue"></div></a></li>
                                    @endif
                                    @if($color->color == 'red' )
                                            <li class="colors"><a href="{{ url('/product-page/'.$color->id) }}"><div class="color-red"></div></a></li>
                                    @endif
                                        @if($color->color == 'green' )
                                            <li class="colors"><a href="{{ url('/product-page/'.$color->id ) }}"><div class="color-green"></div></a></li>
                                    @endif
                                        @if($color->color == 'yellow' )
                                            <li class="colors"><a href="{{ url('/product-page/'.$color->id) }}"><div class="color-yellow"></div></a></li>
                                    @endif
                                        @if($color->color == 'gray' )
                                            <li class="colors"><a href="{{ url('/product-page/'.$color->id) }}"><div class="color-gray"></div></a></li>
                                    @endif
                                        @if($color->color == 'ash' )
                                            <li class="colors"><a href="{{ url('/product-page/'.$color->id) }}"><div class="color-ash"></div></a></li>
                                    @endif
                                        @if($color->color == 'gold' )
                                            <li class="colors"><a href="{{ url('/product-page/'.$color->id) }}"><div class="color-gold"></div></a></li>
                                    @endif
                                    @endforeach
                            </ul>
                            <div class="clr"></div>
                            <div style="margin-top: 20px">
                                <ul class="cart">
                                    <form action="{{ url('/add-to-cart') }}" method="post">
                                        {{ csrf_field() }}
                                    <li class="text"><h3>Quentity</h3></li>
                                    <li class="qntty"><input type="number" name="qty" value="1" min="1" max="{{ $productById->product_quantity }}"></li>
                                        <input type="hidden" name="product_id" value="{{ $productById->id }}" >
                                        <input type="hidden" name="model" value="{{ $productById->code_no }}" >
                                        <input type="hidden" name="size" value="{{ $productById->product_size }}" >
                                        <input type="hidden" name="color" value="{{ $productById->product_color }}" >
                                    <li class="add-cart"><button class="btn-info">Add to Cart</button></li>
                                    </form>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-8 mx-auto pt-5">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-justified">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#panel1" role="tab">Description</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#panel2" role="tab">Size Guide</a>
                        </li>
                    </ul>
                    <!-- Tab panels -->
                    <div class="tab-content card description-card py-5">
                        <!--Panel 1-->
                        <div class="tab-pane fade in show active" id="panel1" role="tabpanel">
                            <br>
                            <p><?php echo $productById->description ?></p>
                        </div>
                        <!--/.Panel 1-->
                        <!--Panel 2-->
                        <div class="tab-pane fade" id="panel2" role="tabpanel">
                            <br>
                            <img class="d-block product-img-select w-100"  src="{{ asset($productById->size_guide ) }}" class="img-fluid">
                        </div>
                        <!--/.Panel 2-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Product Details Descritption Ends-->


    <!--Related Product starts here-->
    <section class="text-center ">
        <div class="container-fluid">
            <div class="row">
                <!-- Section heading -->
                <h4 class="font-weight-bold text-center my-5"><a class="active-link" href="">Related Prosucts </a></h4>
                <!-- Carousel Wrapper -->
                <!-- Carousel Wrapper -->
                <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">
                    <!-- Controls -->
                    <div class="controls-top text-right">
                        <a class="btn-floating waves-effect waves-light" href="#multi-item-example" data-slide="prev">
                            <i class="fa slider-icon fa-chevron-left"></i>
                        </a>
                        <a class="btn-floating  waves-effect waves-light" href="#multi-item-example" data-slide="next">
                            <i class="fa slider-icon fa-chevron-right"></i>
                        </a>
                    </div>
                    <!-- Controls -->
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li class="primary-color active" data-target="#multi-item-example" data-slide-to="0"></li>
                        <li class="primary-color" data-target="#multi-item-example" data-slide-to="1"></li>
                        <li class="primary-color" data-target="#multi-item-example" data-slide-to="2"></li>
                    </ol>
                    <!-- Indicators -->
                    <!-- Slides -->
                    <?php  $products = DB::table('products')->where('sub_category_id',$productById->sub_category_id)->where('status',1)->orderBy('id','desc')->take('18')->get(); ?>
                    <div class="carousel-inner" data-ride="carousel"   role="listbox">
                        <!-- First slide -->
                        @foreach($products->chunk(6) as $count=>$products)
                            <div class="carousel-item{{ $count==0  ? ' active' : '' }}">
                                @foreach($products as $product )
                                    {{--@if($product->code_no != $preModel )--}}
                                    <div class="col-sm-6 col-md-4 col-lg-2 mb-2 p-1">
                                        <!-- Card -->
                                        <div class="card card-cascade custom-card narrower card-ecommerce">
                                            <!-- Card image -->
                                            <div class="view view-cascade overlay">
                                                <div class="view overlay zoom" data-value="" id="image">
                                                    <script>
                                                        if(document.getElemtntById('image')){

                                                        }
                                                    </script>
                                                    <a href="{{ url('/product-page/'.$product->id) }}">
                                                        <img src="{{ asset($product->image) }}" class="img-fluid card-img-top" alt="sample photo">
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Card image -->
                                            <!-- Card content -->
                                            <div class="card-body card-body-cascade text-center">
                                                <div class="card-content">
                                                    <div>
                                                        <div class="float-left">
                                                            <ul class="ul-colors">
                                                                <?php $colors = DB::table('products')->where('code_no',$product->code_no)->where('status',1)->get()?>
                                                                @foreach($colors as $color )
                                                                    @if($color->product_color == 'red')
                                                                        {{ $color->id }}
                                                                        <li class="color-picker"><button value="red" id="{{ $color->id }}" onclick="myColor(event)" class="red"></button></li>
                                                                    @endif
                                                                    @if($color->product_color == 'green')
                                                                        {{ $color->id }}
                                                                        <li class="color-picker"><button value="green" id="{{ $color->id }}" onclick="myColor(event)" class="green"></button></li>
                                                                    @endif
                                                                    @if($color->product_color == 'blue')
                                                                        {{ $color->id }}
                                                                        <li class="color-picker"><button value="blue" id="{{ $color->id }}" onclick="myColor(event)" class="blue"></button></li>
                                                                    @endif
                                                                    @if($color->product_color == 'yellow')
                                                                        {{ $color->id }}
                                                                        <li class="color-picker"><button value="yellow" id="{{ $color->id }}" onclick="myColor(event)" class="yellow"></button></li>
                                                                    @endif
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                        <div class="float-right card-icon">
                                                            <a class="">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                            <a class="" >
                                                                <i class="fa fa-heart"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="clr"></div>
                                                    <div class="text-center">
                                                        <p class="model-no">{{ $product->code_no  }}</p>
                                                        <p class="price">Price {{ $product->product_prize }}/-</p>
                                                        <p class="price" id="test"></p>
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- Card content -->
                                        </div>
                                        <!-- Card -->
                                    </div>
                                    {{--@endif--}}
                                @endforeach
                            </div>
                            <!-- First slide -->
                        @endforeach
                    </div>
                    <!-- Slides -->
                </div>
                <!-- Carousel Wrapper -->
            </div>
        </div>
    </section>
    <!--Related Product Ends here-->
    @endsection