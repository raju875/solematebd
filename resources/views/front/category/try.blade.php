
<div class="infinite-scroll">
@foreach($products as $product )
    <div class="row">
        <div class="col-sm-6 col-md-4 col-lg-2 mb-2 p-1">
            <!-- Card -->
            <div class="card card-cascade custom-card narrower card-ecommerce">
                <!-- Card image -->
                <div class="view view-cascade overlay">
                    <div class="view overlay zoom">
                        <div class="view overlay zoom">
                            <img src="{{ $product->image }}" class="img-fluid card-img-top" alt="sample photo">
                        </div>
                    </div>
                </div>
                <!-- Card image -->
                <!-- Card content -->
                <div class="card-body card-body-cascade text-center">
                    <div class="card-content">
                        <div>
                            <div class="float-left">
                                <ul class="ul-colors">
                                    <li class="color-picker"><a href="" class="red"></a></li>
                                    <li class="color-picker"><a href="" class="green"></a></li>
                                    <li class="color-picker"><a href="" class="blue"></a></li>
                                    <li class="color-picker"><a href="" class="yellow"></a></li>
                                </ul>
                            </div>
                            <div class="float-right card-icon">
                                <a class="">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a class="" >
                                    <i class="fa fa-heart"></i>
                                </a>
                            </div>
                        </div>
                        <div class="clr"></div>
                        <div class="text-center">
                            <p class="model-no">{{ $product->code_no }}</p>
                            <p class="price">Price {{ $product->product_prize }}/-</p>
                        </div>


                    </div>
                </div>
                <!-- Card content -->
            </div>
            <!-- Card -->
        </div></div>
    @endforeach


    {{ $products->links() }}
    </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{{ asset('admin/jscroll/jscroll-master/dist/') }}/jquery.jscroll.min.js"></script>
<script type="text/javascript">
    $('ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<img class="center-block" src="/images/loading.gif" alt="Loading..." />',
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
            }
        });
    });
</script>