@extends('front.master')

@section('title')
    @foreach($cats as $cat )
        {{ $cat->category_name }}  | Solemate
    @endforeach

@endsection


@section('content')
    <!--Banner Starts here-->
    <section id="banner">
        <div class="container-fluid">
            <div class="row">
                <div class="col p-0">
                    @foreach($cats as $cat )
                    <div class="view banner-view">
                        <img height="350" src="{{ $cat->image }}" class="img-fluid w-100" alt="Image of ballons flying over canyons with mask pattern one.">
                        <div class="mask flex-center waves-effect waves-light">
                            <h1 class="white-text font-weight-bold">
                                    {{ $cat->category_name }}
                            </h1>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <div class="clr"></div>
    <!--Banner Ends here-->
    <section id="category-product">
        <div class="container-fluid">
            <div class="d-block w-100 text-left">
                {{--<h4 class="font-weight-bold text-left my-5" style="height: 80px;"><a class="active-link" href="">Men </a>/<a class="disabled-link" href=""> Best Selling </a>/<a class="disabled-link" href=""> New Arrivals </a></h4>--}}
            </div>
            @foreach($products->chunk(6) as $product)
                <div class="row">
                    @foreach($product as $product )
                <div class="col-sm-6 col-md-4 col-lg-2 mb-2 p-1">
                    <!-- Card -->
                    <div class="card card-cascade custom-card narrower card-ecommerce">
                        <!-- Card image -->
                        <a href="{{ url('/product-page/'.$product->id ) }}">
                        <div class="view view-cascade overlay">
                            <div class="view overlay zoom">
                                <div class="view overlay zoom">
                                    <img src="{{ $product->image }}" class="img-fluid card-img-top" alt="sample photo">
                                </div>
                            </div>
                        </div>
                        </a>
                        <!-- Card image -->
                        <!-- Card content -->
                        <div class="card-body card-body-cascade text-center">
                            <div class="card-content">
                                <div>
                                    <div class="float-left">
                                        <ul class="ul-colors">
                                            <?php $colors = DB::table('products')->where('code_no',$product->code_no)->where('status',1)->get()?>
                                            @foreach($colors as $color )
                                                @if($color->product_color == 'red')
                                                    <li class="color-picker"><a href="{{ url('/product-page/'.$product->id) }}" class="red"></a></li>
                                                @endif
                                                @if($color->product_color == 'green')
                                                    <li class="color-picker"><a href="{{ url('/product-page/'.$product->id) }}" class="green"></a></li>
                                                @endif
                                                @if($color->product_color == 'blue')
                                                    <li class="color-picker"><a href="{{ url('/product-page/'.$product->id) }}" class="blue"></a></li>
                                                @endif
                                                @if($color->product_color == 'yellow')
                                                    <li class="color-picker"><a href="{{ url('/product-page/'.$product->id) }}" class="yellow"></a></li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="float-right card-icon">
                                        <a class="">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a class="" >
                                            <i class="fa fa-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="clr"></div>
                                <div class="text-center">
                                    <p class="model-no">{{ $product->code_no }}</p>
                                    <p class="price">Price {{ $product->product_prize }}/-</p>
                                </div>


                            </div>
                        </div>
                        <!-- Card content -->
                    </div>
                    <!-- Card -->
                </div>
                    @endforeach
                </div>
                @endforeach
            </div>
    </section>
    <!--Category Products Goes here-->

@endsection