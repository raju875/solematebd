@extends('front.master')

@section('title')
    Shoping Cart | Solemate
    @endsection

@section('content')
    <!--Shopping-cart-table Starts-->
    <section id="shopping-cart-table">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">

                            <!-- Shopping Cart table -->
                            <div class="table-responsive">

                                <table class="table product-table">

                                    <!-- Table head -->
                                    <thead class="mdb-color lighten-5">
                                    <tr>
                                        <th></th>
                                        <th class="font-weight-bold">
                                            <strong>Product</strong>
                                        </th>
                                        <th class="font-weight-bold">
                                            <strong>Model</strong>
                                        </th>
                                        <th class="font-weight-bold">
                                            <strong>Color</strong>
                                        </th>
                                        <th class="font-weight-bold">
                                            <strong>Size</strong>
                                        </th>
                                        <th class="font-weight-bold">
                                            <strong>Price</strong>
                                        </th>
                                        <th class="font-weight-bold">
                                            <strong>QTY</strong>
                                        </th>
                                        <th class="font-weight-bold">
                                            <strong>Amount</strong>
                                        </th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <!-- /.Table head -->

                                    <!-- Table body -->
                                    <tbody>

                                    <!-- First row -->
                                    <?php $grandTotal=0 ?>
                                    <?php $totalItems =0 ?>
                                    @forelse($products as $product )
                                    <tr>
                                        <th scope="row">
                                            <img src="{{ asset($product->options->image ) }}" alt="" class="img-fluid z-depth-0">
                                        </th>
                                        <td>
                                            <h5 class="mt-3">
                                                <strong>{{ $product->name }}</strong>
                                            </h5>
                                            <p class="text-muted">{{ $product->options->brand_name }}</p>
                                        </td>
                                        <td>{{ $product->options->product_model }}</td>
                                        <td>{{ $product->options->color }}</td>
                                        <td>{{ $product->options->product_size }}</td>
                                        <td>{{ $product->price }}</td>
                                        <td>
                                            <form action="{{ url('/update-cart-product-quantity') }}" method="POST">
                                                @csrf
                                            <input type="hidden" value="{{ $product->rowId }}" name="rowId">
                                            <input type="number" value="{{  $product->qty }}" min="1" max="{{ $product->options->product_quantity }}" name="qty" aria-label="Search" class="form-control" style="width: 100px">
                                            <input type="submit" value="Update">
                                            </form>
                                        </td>
                                        <td class="font-weight-bold">
                                            <?php $total = $product->qty*$product->price?>
                                            <?php $totalItems = $product->qty+$totalItems?>
                                            <strong>{{ $total  }}</strong>
                                        </td>
                                        <td>
                                            <a href="{{ url('/delete-cart-product/'.$product->rowId) }}">
                                            <button type="button" class="btn btn-sm btn-primary" onclick="return confirm('Are you sure to remove it !!!')" data-toggle="tooltip" data-placement="top" title="Remove item">X
                                            </button>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php $grandTotal = $grandTotal+$total ?>
                                    @empty
                                        <h2 class="text-center text-danger">No item select . Please add item </h2>
                                    <!-- /.First row -->
                                    @endforelse

                                    <tr>
                                        <td colspan="3" class="text-right">
                                            <a href="{{ url('/') }}">
                                            <button type="button" class="btn btn-danger btn-rounded">
                                                <i class="fa fa-angle-left left"></i>Continue Shopping
                                            </button>
                                            </a>
                                        </td>
                                        <td>
                                            <h4 class="mt-2">
                                                <strong>Total</strong>
                                            </h4>
                                        </td>
                                        <td class="text-right">
                                            <h4 class="mt-2">
                                                <strong>{{ $grandTotal }}/= </strong>
                                                {{ Session::put('grandTotal',$grandTotal) }}
                                                {{ Session::put('totalItems',$totalItems) }}
                                            </h4>
                                        </td>
                                        @if($totalItems!=0)
                                            @if(Session::get('customerId'))
                                                <td colspan="3" class="text-right">
                                                    <a href="{{ url('/order-info') }}">
                                                        <button type="button" class="btn btn-primary btn-rounded">
                                                            <i class="fa fa-angle-right right"></i>Complete purchase
                                                        </button>
                                                    </a>
                                                </td>
                                                @else
                                        <td colspan="3" class="text-right">
                                            <a href="{{ url('/checkout') }}">
                                            <button type="button" class="btn btn-primary btn-rounded">
                                                <i class="fa fa-angle-right right"></i>Complete purchase
                                            </button>
                                            </a>
                                        </td>
                                                @endif
                                            @endif
                                    </tr>
                                    </tbody>
                                    <!-- /.Table body -->

                                </table>

                            </div>
                            <!-- /.Shopping Cart table -->

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Shopping-cart-table Ends-->

    @endsection