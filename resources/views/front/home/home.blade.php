@extends('front.master')

@section('title')
    Home | Solemate
@endsection

@section('banner')
    @include('front.includes.banner',array('banners' => DB::table('banners')->where('status',1)->orderBy('id','desc')->take(1)->get(),
                                    'advertisements' => DB::table('advertisements')->where('status',1)->orderBy('id','desc')->take(1)->get()) )
@endsection

@section('content')
    <!--men Product starts here-->
    <section class="text-center ">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <?php $categories = \App\Category::where('status',1)->orderBy('id','desc')->get()?>
                    @foreach($categories as $category )
                    <a  href="{{ url('/'.$category->category_name.'-products') }}" style="color: #000;font-weight: bold" class="text-left" aria-controls="men" aria-selected="true">{{ $category->category_name }}</a>
                    <div id="multi-item-example1" class="carousel slide carousel-multi-item" data-ride="carousel" data-interval="false">
                                <!-- Controls -->
                                <div class="controls-top pb-0 text-right">
                                    <a class="btn-floating waves-effect waves-light" href="#multi-item-example1" data-slide="prev">
                                        <i class="fa slider-icon fa-chevron-left"></i>
                                    </a>
                                    <a class="btn-floating  waves-effect waves-light" href="#multi-item-example1" data-slide="next">
                                        <i class="fa slider-icon fa-chevron-right"></i>
                                    </a>
                                </div>
                                <!-- Controls -->

                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li class="primary-color active" data-target="#multi-item-example1" data-slide-to="0"></li>


                                </ol>
                                <!-- Indicators -->
                                <!-- Slides -->

                                    <?php  $menProducts =  DB::table('products')
                                                           ->join('sub_categories','products.sub_category_id','=','sub_categories.id')
                                                           ->join('categories','sub_categories.category_id','=','categories.id')
                                                           ->join('colors','products.product_color','=','colors.id')
                                                           ->where('categories.category_name',$category->category_name) ->where('products.status',1)
                                                           ->select('products.*')->orderBy('id','desc')->take('18')->get(); ?>
                                    <div class="carousel-inner" data-ride="carousel"   role="listbox">
                                        <!-- First slide -->
                                        @foreach($menProducts->chunk(6) as $count=>$products)
                                            <div class="carousel-item{{ $count==0  ? ' active' : '' }}">
                                                @foreach($products as $product )
                                                    {{--@if($product->code_no != $preModel )--}}
                                                    <div class="col-sm-6 col-md-4 col-lg-2 mb-2 p-1">
                                                        <!-- Card -->
                                                        <div class="card card-cascade custom-card narrower card-ecommerce">
                                                            <!-- Card image -->
                                                            <div class="view view-cascade overlay">
                                                                <div class="view overlay zoom" data-value="" id="image">
                                                                    <script>
                                                                        if(document.getElemtntById('image')){

                                                                        }
                                                                    </script>
                                                                    <a href="{{ url('/product-page/'.$product->id) }}">
                                                                        <img src="{{ asset($product->image) }}" class="img-fluid card-img-top" alt="sample photo">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!-- Card image -->
                                                            <!-- Card content -->
                                                            <div class="card-body card-body-cascade text-center">
                                                                <div class="card-content">
                                                                    <div class="clr"></div>
                                                                    <div class="text-center">
                                                                        <p class="model-no">{{ $product->code_no  }}</p>
                                                                        <p class="price">Price {{ $product->product_prize }}/-</p>
                                                                        <p class="price" id="test"></p>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <!-- Card content -->
                                                        </div>
                                                        <!-- Card -->
                                                    </div>
                                                    {{--@endif--}}
                                                @endforeach
                                            </div>
                                            <!-- First slide -->
                                            @endforeach
                                    </div>
                            {{--@endif--}}
                                    <!-- Slides -->

                            </div>

@endforeach

                </div>
            </div>
        </div>
    </section>

    <script>
        $('.carousel').carousel({
            interval: false
        })
    </script>

    <!--color script  start here-->
        <script>
            function myColor(event) {
                //var selectColor = document.getElementsByClassName('');
                // var color = document.getElementById('color').value;
                //alert(event.target.value);
                var productId = event.target.id;
                var color = document.getElementById(productId).value;
                var xmlHttp = new XMLHttpRequest();
                var serverPage = 'http://localhost:8080/sajib-ecommerce/public/product-color-select/'+productId+'/'+color;
                xmlHttp.open('GET',serverPage);
                xmlHttp.onreadystatechange = function () {
                    if (!(xmlHttp.readyState == 4 && xmlHttp.status == 200)) {
                        return;
                    }
                    // success : function (response) {
                   document.getElementById('image').innerHTML = xmlHttp.responseText;
                    //     $("#clicked").html(response)
                    // }
                }
                xmlHttp.send(null);
            }

        </script>
    <!--color script end here-->
@endsection
