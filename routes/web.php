<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ---------- Exception handling Start ----------
Route::get('/404','ErrorHandleController@error404');
Route::get('/405','ErrorHandleController@error405');
// ---------- Exception handling End ----------


Route::get('/my-ecommerce-administration/dashboard','AdminMasterController@index');

// ---------- Profile Start ----------
Route::get('/my-ecommerce-administration/profile/profile','ProfileController@changeProfile')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/profile/update-profile','ProfileController@updateProfile')->middleware('AuthenticateMiddleware');
// ---------- Profile End ----------


// ---------- Message Start ----------
Route::get('/my-ecommerce-administration/message/message-form','MessageController@messageForm')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/message/message-add','MessageController@addNewMessage')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/message/message-table','MessageController@messageManage')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/message/unpublished-message/{id}','MessageController@unpublishedMessage')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/message/published-message/{id}','MessageController@publishedMessage')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/message/editable-message-form/{id}','MessageController@messageEditableForm')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/message/update-message','MessageController@updateMessage')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/message/delete-message-info/{id}','MessageController@deleteMessage')->middleware('AuthenticateMiddleware');
// ---------- Message End ----------


// ---------- Logo Start ----------
Route::get('/my-ecommerce-administration/logo/logo-form','LogoController@logoForm')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/logo/logo-add','LogoController@addNewLogo')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/logo/logo-table','LogoController@logoManage')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/logo/unpublished-logo/{id}','LogoController@unpublishedLogo')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/logo/published-logo/{id}','LogoController@publishedLogo')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/logo/editable-logo-form/{id}','LogoController@logoEditableForm')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/logo/update-logo','LogoController@updateLogo')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/logo/delete-logo-info/{id}','LogoController@deleteLogo')->middleware('AuthenticateMiddleware');
// ---------- Logo End ----------


// ---------- Banner Start ----------
Route::get('/my-ecommerce-administration/banner/banner-form','BannerController@bannerForm')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/banner/banner-add','BannerController@addNewBanner')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/banner/banner-table','BannerController@bannerManage')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/banner/unpublished-banner/{id}','BannerController@unpublishedBanner')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/banner/published-banner/{id}','BannerController@publishedBanner')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/banner/editable-banner-form/{id}','BannerController@bannerEditableForm')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/banner/update-banner','BannerController@updateBanner')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/banner/delete-banner-info/{id}','BannerController@deleteBanner')->middleware('AuthenticateMiddleware');
// ---------- Banner End ----------


// ---------- Add Start ----------
Route::get('/my-ecommerce-administration/ad/ad-form','AdvertisementController@adForm')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/ad/ad-save','AdvertisementController@adSave')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/ad/ad-table','AdvertisementController@adManageTable')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/ad/unpublished-ad/{id}','AdvertisementController@unpublishedAd')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/ad/published-ad/{id}','AdvertisementController@publishedAd')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/ad/editable-ad-form/{id}','AdvertisementController@adEditableForm')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/ad/update-ad','AdvertisementController@updateAd')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/ad/delete-ad-info/{id}','AdvertisementController@deleteAdInfo')->middleware('AuthenticateMiddleware');
// ---------- Add End ----------


// ---------- Category Start ----------
Route::get('/my-ecommerce-administration/category/add-category-form','CategoryController@categoryForm')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/category/category-add','CategoryController@addCategory')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/category/category-table','CategoryController@manageCategory')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/category/unpublished-category/{id}','CategoryController@unpublishedCategory')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/category/published-category/{id}','CategoryController@publishedCategory')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/category/editable-category-form/{id}','CategoryController@editableCategoryForm')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/category/update-category','CategoryController@updateCategory')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/category/delete-category-info/{id}','CategoryController@deleteCategoryInfo')->middleware('AuthenticateMiddleware');
// ---------- Category End ----------


// ---------- Sub Category Start ----------
Route::get('/my-ecommerce-administration/sub-category/sub-category-form','SubCategoryController@subCategoryForm')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/sub-category/sub-category-add','SubCategoryController@addNewSubCategory')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/sub-category/sub-category-table','SubCategoryController@manageSubCategory');
Route::get('/my-ecommerce-administration/sub-category/unpublished-sub-category/{id}','SubCategoryController@unpublishedSubCategory')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/sub-category/published-sub-category/{id}','SubCategoryController@publishedSubCategory')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/sub-category/editable-sub-category-form/{id}','SubCategoryController@editableSubCategoryForm')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/sub-category/update-sub-category','SubCategoryController@updateSubCategory')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/sub-category/delete-sub-category-info/{id}','SubCategoryController@deleteSubCategoryInfo')->middleware('AuthenticateMiddleware');
// ---------- Sub Category End ----------


// ---------- Brand Start ----------
Route::get('/my-ecommerce-administration/brand/brand-form','BrandController@brandForm')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/brand/brand-add','BrandController@addNewBrand')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/brand/brand-table','BrandController@manageBrand');
Route::get('/my-ecommerce-administration/brand/unpublished-brand/{id}','BrandController@unpublishedBrand')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/brand/published-brand/{id}','BrandController@publishedBrand')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/brand/editable-brand-form/{id}','BrandController@editableBrandForm')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/brand/update-brand','BrandController@updateBrand')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/brand/delete-brand-info/{id}','BrandController@deleteBrandInfo')->middleware('AuthenticateMiddleware');
// ---------- Brand End ----------


// ---------- Color Start ----------
Route::get('/my-ecommerce-administration/color/color-form','ColorController@colorForm')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/color/color-add','ColorController@addNewColor')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/color/color-table','ColorController@manageColor');
Route::get('/my-ecommerce-administration/color/unpublished-color/{id}','ColorController@unpublishedColor')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/color/published-color/{id}','ColorController@publishedColor')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/color/editable-color-form/{id}','ColorController@editableColorForm')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/color/update-color','ColorController@updateColor')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/color/delete-color-info/{id}','ColorController@deleteColorInfo')->middleware('AuthenticateMiddleware');
// ---------- Color End ----------

// ---------- Product Start ----------
Route::get('/my-ecommerce-administration/product/product-form','ProductController@productForm')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/ajax-dynamic-dependency-dropdown/{catId}','ProductController@dropdownDependency')->middleware('AuthenticateMiddleware');

Route::post('/my-ecommerce-administration/product/add-new-product','ProductController@addNewProduct')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/product/product-table','ProductController@productTable')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/product/unpublished-product/{id}','ProductController@unpublishedProduct')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/product/published-product/{id}','ProductController@publishedProduct')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/product/editable-product-form/{id}','ProductController@editableProductForm')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/product/update-product','ProductController@updateProduct')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/product/delete-product-sub-image/{id}','ProductController@deleteProductSubImage')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/product/delete-product-info/{id}','ProductController@deleteProductInfo')->middleware('AuthenticateMiddleware');
// ---------- Product End ----------

// ---------- Order Start ----------
// ----------  (Pending)----------
Route::get('/my-ecommerce-administration/order/pending-order','OrderManageController@pendingOrder')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/order/order-details/{id}','OrderManageController@orderDetails')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/invoice/invoice-view/{id}','OrderManageController@viewInvoice')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/invoice/pdf/{id}','OrderManageController@pdfInvoice')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/order/confirm-order/{id}','OrderManageController@orderConfirm')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/order/order-cancel/{id}','OrderManageController@orderCancel')->middleware('AuthenticateMiddleware');

// ---------- (Successful)----------
Route::get('/my-ecommerce-administration/order/successful-delivery','OrderManageController@successfulDelivery')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/order/deliver-details/{id}','OrderManageController@deliverDetails')->middleware('AuthenticateMiddleware');
Route::get('/my-ecommerce-administration/order/delete-delivery-info/{id}','OrderManageController@deleteDeleveryInfo')->middleware('AuthenticateMiddleware');
// ---------- Order End ----------

// ---------- Stock Start ----------
Route::get('/my-ecommerce-administration/stock/stock-report','StockController@stockReport')->middleware('AuthenticateMiddleware');
Route::post('/my-ecommerce-administration/stock/update-stock','StockController@updateStock')->middleware('AuthenticateMiddleware');
Route::get('/live_search/action', 'StockController@action')->name('live_search.action');#
// ---------- Stock End ---------

// ---------- Error Start ----------
Route::get('/my-ecommerce-administration/error/plain-error','ErrorController@plainError');
// ---------- Error End ----------

-

Auth::routes();

Route::get('/my-ecommerce-administration/home', 'HomeController@index')->name('home');


// ---------------------------------------Front---------------------------------------------//

Route::get('/','WelcomeController@index');
Route::get('/{category}-products','WelcomeController@categoryPage');
Route::get('/product-page/{id}','WelcomeController@productPage');
Route::get('/product-page/{id}/color_{color}','WelcomeController@productPageColor');
Route::get('/product-page/model_{productId}/size_{size}','WelcomeController@selectProductSize');
Route::get('/product-page/model_{productId}/size_{size}/color_{color}','WelcomeController@selectProductColorBySize');

// ---------- Registration ----------
Route::get('/user-login','CheckoutController@checkout');
Route::post('/user-login-check','CheckoutController@loginCheck');
Route::post('/register-to-order','CheckoutController@registerToOrder');
Route::post('/login-to-order','CheckoutController@loginToOrder');
Route::post('/user-logout','CheckoutController@logout');

// ---------- Shoping Cart ----------
Route::post('/add-to-cart','ShopingCartController@addToCart');
Route::get('/show-cart','ShopingCartController@showCart');
Route::post('/update-cart-product-quantity','ShopingCartController@updateCartProductQuantity');
Route::get('/delete-cart-product/{id}','ShopingCartController@deleteCartProduct');

// ---------- Checkout ----------
Route::get('/checkout','CheckoutController@checkout');
Route::post('/register','CheckoutController@register');
Route::get('/order-info','CheckoutController@orderInfo');
Route::post('/order-confirm','CheckoutController@confirmOrder');


Route::get('/reload-new-arrival-products','WelcomeController@newArrivalProducts');
Route::get('/{link}', function($link) {
    return view('front.home.home',['link' => $link]);
});

