<?php

namespace App\Http\Controllers;

use App\Color;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ColorController extends Controller
{
    public function colorForm () {
        return view('admin.color.color-form');
    }

    public function addNewColor (Request $request) {
        //return $request->all();
        $this->validate($request, [
            'color'  => 'required',
            'status'  => 'required'
        ]);

        $color = new Color();
        $color->color = $request->color;
        $color->status = $request->status;
        $color->save();
        return redirect('/my-ecommerce-administration/color/color-form')->with('message','Color added successfully');
    }

    public function manageColor () {
        return view('admin.color.manage-color');
    }

    public function unpublishedColor($id)
    {
        DB::table('colors')->where('id', $id)->update(['status' => 0]);
        return redirect('/my-ecommerce-administration/color/color-table')->with('message', 'Unpublished color info successfully');
    }

    public function publishedColor($id)
    {
        DB::table('colors')->where('id', $id)->update(['status' => 1]);
        return redirect('/my-ecommerce-administration/color/color-table')->with('message', 'Published color info successfully');
    }

    public function editableColorForm ($id) {
        $colorById = Color::find($id);
        return view('admin.color.editable-color-form', [
            'colorById' => $colorById
        ]);
    }

    public function updateColor (Request $request) {
        $this->validate($request, [
            'color'  => 'required',
            'status'  => 'required'
        ]);
        $color =  Color::find($request->id);
        $color->color = $request->color;
        $color->status = $request->status;
        $color->save();
        return redirect('/my-ecommerce-administration/color/color-table')->with('message','Color update successfully');
    }

    public function deleteColorInfo($id) {
        Color::find($id)->delete();
        return redirect('/my-ecommerce-administration/color/color-table')->with('message','Color delete successfully');

    }
}
