<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advertisement;
use Image;
use Illuminate\Support\Facades\DB;

class AdvertisementController extends Controller
{
   public function adForm() {
       return view('admin.ad.ad-form');
   }

    public function adSave(Request $request)
    {
        //return $request->all();
        $this->validate($request, [
            'ad_image' => 'required',
            'status'    => 'required'
        ]);

        $adsImage      = $request->file('ad_image');
        $adsImageName  = $adsImage->getClientOriginalName();
        $directory     = 'admin/addvertisements/';
        $adsUrl        = $directory . $adsImageName;
        Image::make($adsImage)->save($adsUrl);

        $ads = new Advertisement();
        $ads->ad_name     = $request->add_name;
        $ads->description = $request->description;
        $ads->ad_image    = $adsUrl;
        $ads->status      = $request->status;

        $ads->save();

        return redirect('/my-ecommerce-administration/ad/ad-form')->with('message', 'New advertisement added successfully');
    }

    public function adManageTable() {
        $ads = DB::table('advertisements')->orderBy('id', 'desc')->get();
        return view('admin.ad.ad-manage-table', [
            'ads' => $ads
        ]);
    }

    public function unpublishedAd($id ) {
        DB::table('advertisements')->where('id', $id)->update(['status' => 0]);
        return redirect('/my-ecommerce-administration/ad/ad-table')->with('message', 'Unpublishes Ads info successfully');
    }

    public function publishedAd($id ) {
        DB::table('advertisements')->where('id', $id)->update(['status' => 1]);
        return redirect('/my-ecommerce-administration/ad/ad-table')->with('message', 'Publishes Ads info successfully');
    }

    public function adEditableForm($id){
        $adByid = Advertisement::find($id);
        return view('admin.ad.editable-ad-form', [
            'adByid' => $adByid
        ]);
    }

    public function updateAd(Request $request)
    {
        // return $request->all();

        if ($request->file('ad_image') == null) {
            $ad = Advertisement::find($request->id);
            $ad->ad_name     = $request->ad_name;
            $ad->description = $request->description;
            $ad->status      = $request->status;

            $ad->save();

            return redirect('/my-ecommerce-administration/ad/ad-table')->with('message', 'Update Ads info successfully');
        }

        $adsImage      = $request->file('ad_image');
        $adsImageName  = $adsImage->getClientOriginalName();
        $directory     = 'admin/addvertisements/';
        $adsUrl        = $directory . $adsImageName;
        Image::make($adsImage)->save($adsUrl);

        $ads = Advertisement::find($request->id);

        $ads->ad_name     = $request->add_name;
        $ads->description = $request->description;
        $ads->ad_image    = $adsUrl;
        $ads->status      = $request->status;

        $ads->save();

        return redirect('/my-ecommerce-administration/ad/ad-table')->with('message', 'Update Ads info successfully');
    }

    public function deleteAdInfo($id )
    {
        Advertisement::find($id)->delete();
        return redirect('/my-ecommerce-administration/ad/ad-table')->with('message', 'Delete Ads info successfully');
    }
}
