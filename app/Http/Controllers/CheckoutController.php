<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Order;
use App\OrderDetail;
use App\ShippingInfo;
use Cart;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public function checkout() {
        return view('front.checkout.checkout');
    }

    public function  register(Request $request ) {
        //return $request->all();

        $customer = new Customer();
        $customer->name      = $request->name;
        $customer->email     = $request->email;
        $customer->password  = bcrypt($request->password);
        $customer->mobile_no = $request->mobile_no;
        $customer->save();

        Session::put('customerId',$customer->id);
        Session::put('customerEmail',$customer->email);

        return redirect('/login')->with('message','Registration successfully');
    }

    public function loginCheck(Request $request) {
       $count = Customer::where('email',$request->email)->count();
       if ($count==1){
           $validCustomer =  Customer::where('email',$request->email)->first();
           if (password_verify($request->password,$validCustomer->password)) {
               Session::put('customerId',$validCustomer->id);
               Session::put('customerEmail',$validCustomer->email);

               return redirect('/login')->with('message','Successfully login');
           }else {
               return redirect('/login')->with('alert','Wrong password');
           }
       }else {
           return redirect('/login')->with('alert','Please register first');
       }

    }

    public function registerToOrder(Request $request ) {
        $customer = new Customer();
        $customer->name      = $request->name;
        $customer->email     = $request->email;
        $customer->password  = bcrypt($request->password);
        $customer->mobile_no = $request->mobile_no;
        $customer->save();

        Session::put('customerId',$customer->id);
        Session::put('customerEmail',$customer->email);

        return redirect('/order-info');
    }

    public function loginToOrder(Request $request) {
        $count = Customer::where('email',$request->email)->count();
        if ($count==1){
            $validCustomer =  Customer::where('email',$request->email)->first();
            if (password_verify($request->password,$validCustomer->password)) {
                Session::put('customerId',$validCustomer->id);
                Session::put('customerEmail',$validCustomer->email);

                return redirect('/order-info');
            }else {
                return redirect('/user-login')->with('alert','Wrong password');
            }
        }else {
            return redirect('/user-login')->with('alert','Please register first');
        }

    }

    public function logout(Request $request ) {
        Session::forget('customerId');
        Session::forget('customerName');
        Session::forget('grandTotal');
        Cart::destroy();

        return redirect('/');
    }

    public function orderInfo() {
        return view('front.checkout.order-info');
    }

    public function confirmOrder(Request $request) {
        $this->validate($request, [
            'first_name'     => 'required|regex:/^[\pL\s\-]+$/u',
            'last_name'      => 'required|regex:/^[\pL\s\-]+$/u',
            'email'          => 'required',
            'street_address' => 'required',
            'village'        => 'required',
            'district'       => 'required',
            'sub_district'   => 'required',
            'zip'            => 'required',
            'mobile_no'      => 'required|size:11|regex:/(01)[0-9]{9}/'
        ]);

        $shippingInfo = new ShippingInfo();
        $shippingInfo->customer_id    = Session::get('customerId');
        $shippingInfo->first_name     = $request->first_name;
        $shippingInfo->last_name      = $request->last_name;
        $shippingInfo->email          = $request->email;
        $shippingInfo->street_address = $request->street_address;
        $shippingInfo->village        = $request->village;
        $shippingInfo->district       = $request->district;
        $shippingInfo->sub_district   = $request->sub_district;
        $shippingInfo->zip            = $request->zip;
        $shippingInfo->mobile_no      = $request->mobile_no;

        $shippingInfo->save();

        $order = new Order();
        $order->customer_id  = Session::get('customerId');
        $order->shipping_id  = $shippingInfo->id;
        $order->order_total  = Session::get('grandTotal');
        $order->save();

        $products = Cart::content();
        foreach ($products as $product ) {
            $orderDetail = new OrderDetail();
            $orderDetail->order_id         = $order->id;
            $orderDetail->product_id       = $product->id;
            $orderDetail->product_name     = $product->name;
            $orderDetail->brand_name       = $product->options->brand_name;
            $orderDetail->product_model    = $product->options->product_model;
            $orderDetail->product_color    = $product->options->color;
            $orderDetail->product_size     = $product->options->product_size;
            $orderDetail->product_quantity = $product->qty;
            $orderDetail->product_price    = $product->price;
            $orderDetail->product_image    = $product->options->image;
            $orderDetail->save();

            Session::forget('grandTotal');
            Cart::destroy();

        }
        return redirect('/order-info')->with('message','Thank you for your valuable information. Soon we process it .');
    }

}
