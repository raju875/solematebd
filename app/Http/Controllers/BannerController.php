<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;
use Illuminate\Support\Facades\DB;
use Image;
class BannerController extends Controller
{
    public function bannerForm()
    {
        return view('admin.banner.banner-form');
    }

    public function addNewBanner(Request $request)
    {
        //return $request->all();
        $this->validate($request, [
            'banner' => 'required',
            'status' => 'required'
        ]);

        $banner = $request->file('banner');
        $bannerName = $banner->getClientOriginalName();
        $directory = 'admin/main-banners/';
        $bannerUrl = $directory . $bannerName;
        Image::make($banner)->save($bannerUrl);

        $Banner = new Banner();
        $Banner->banner_name = $request->banner_name;
        $Banner->description = $request->description;
        $Banner->banner = $bannerUrl;
        $Banner->status = $request->status;

        $Banner->save();

        return redirect('/my-ecommerce-administration/banner/banner-form')->with('message', 'Banner added successfully');
    }

    public function bannerManage()
    {
        $banners = DB::table('banners')->orderBy('id', 'desc')->get();
        return view('admin.banner.manage-banner', [
            'banners' => $banners
        ]);

    }

    public function unpublishedBanner($id)
    {
        DB::table('banners')->where('id', $id)->update(['status' => 0]);

        return redirect('/my-ecommerce-administration/banner/banner-table')->with('message', 'Unpublished banner info successfully');
    }

    public function publishedBanner($id)
    {
        DB::table('banners')->where('id', $id)->update(['status' => 1]);

        return redirect('/my-ecommerce-administration/banner/banner-table')->with('message', 'Published banner info successfully');
    }

    public function bannerEditableForm($id)
    {
        $bannerByid = Banner::find($id);
        return view('admin.banner.editable-banner-form', [
            'bannerByid' => $bannerByid
        ]);
    }

    public function updateBanner(Request $request)
    {
        // return $request->category_name;

        if ($request->file('banner') == null) {
            $banner = Banner::find($request->id);
            $banner->banner_name = $request->banner_name;
            $banner->description = $request->description;
            $banner->status      = $request->status;

            $banner->save();

            return redirect('/my-ecommerce-administration/banner/banner-table')->with('message', 'Banner info update successfully');
        }

        $banner = $request->file('banner');
        $bannerName = $banner->getClientOriginalName();
        $directory = 'admin/main-banners/';
        $bannerUrl = $directory . $bannerName;
        Image::make($banner)->save($bannerUrl);

        $banner = Banner::find($request->id);

        $banner->banner_name = $request->banner_name;
        $banner->description = $request->description;
        $banner->banner = $bannerUrl;
        $banner->status = $request->status;

        $banner->save();

        return redirect('/my-ecommerce-administration/banner/banner-table')->with('message', 'Banner info update successfully');
    }

    public function deleteBanner($id )
    {
        Banner::find($id)->delete();
        return redirect('/my-ecommerce-administration/banner/banner-table')->with('message', 'Banner info delete successfully');
    }
}
