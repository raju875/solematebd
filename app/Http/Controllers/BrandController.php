<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;

class BrandController extends Controller
{
    public function brandForm() {
        return view('admin.brand.brand-form');
    }

    public function addNewBrand(Request $request ) {
        //return $request->all();
        $this->validate($request, [
            'brand_name' => 'required',
            'status'     => 'required'
        ]);
        $brand = new Brand();
        if ($request->file('image')!=null ) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalName();
            $directory = 'admin/brand-images/';
            $imageUrl = $directory . $imageName;
            Image::make($image)->save($imageUrl);

            $brand->brand_name  = $request->brand_name;
            $brand->description = $request->description;
            $brand->image       = $imageUrl;
            $brand->status      = $request->status;

            $brand->save();

            return redirect('/my-ecommerce-administration/brand/brand-form')->with('message', 'Brand added successfully');
        }else {
            $brand->brand_name  = $request->brand_name;
            $brand->description = $request->description;
            $brand->status      = $request->status;

            $brand->save();

            return redirect('/my-ecommerce-administration/brand/brand-form')->with('message', 'Brand added successfully');
            }
    }

    public function manageBrand() {
        $brands = DB::table('brands')->orderBy('id','desc')->get();
        return view('admin.brand.manage-brand',[
            'brands' => $brands
        ]);
    }


    public function unpublishedBrand($id)
    {
        DB::table('brands')->where('id', $id)->update(['status' => 0]);
        return redirect('/my-ecommerce-administration/brand/brand-table')->with('message', 'Unpublished brand info successfully');
    }

    public function publishedBrand($id)
    {
        DB::table('brands')->where('id', $id)->update(['status' => 1]);
        return redirect('/my-ecommerce-administration/brand/brand-table')->with('message', 'Published brand info successfully');
    }

    public function editableBrandForm($id)
    {
        $brandById = Brand::find($id);
        return view('admin.brand.editable-brand-form', [
            'brandById' => $brandById
        ]);
    }

    public function updateBrand(Request $request)
    {
        // return $request->category_name;
        $this->validate($request, [
            'brand_name' => 'required',
            'status'     => 'required'
        ]);
        if ($request->file('image') == null) {
            $brand = Brand::find($request->id);
            $brand->brand_name  = $request->brand_name;
            $brand->description = $request->description;
            $brand->status      = $request->status;

            $brand->save();

            return redirect('/my-ecommerce-administration/brand/brand-table')->with('message', 'Brand info update successfully');
        }

        $image = $request->file('image');
        $imageName = $image->getClientOriginalName();
        $directory = 'admin/brand-images/';
        $imageUrl = $directory . $imageName;
        Image::make($image)->save($imageUrl);

        $brand = Brand::find($request->id);
        $brand->brand_name  = $request->brand_name;
        $brand->description = $request->description;
        $brand->image       = $imageUrl;
        $brand->status      = $request->status;

        $brand->save();

        return redirect('/my-ecommerce-administration/brand/brand-table')->with('message', 'Brand info update successfully');
    }
    public function deleteBrandInfo($id)
    {
        Brand::find($id)->delete();
        return redirect('/my-ecommerce-administration/brand/brand-table')->with('message', 'Brand info delete successfully');
    }

}

