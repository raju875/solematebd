<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Order;
use App\OrderDetail;
use App\Product;
use App\ShippingInfo;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderManageController extends Controller
{
    public function pendingOrder() {
        $orders = Order::all();
        $orders = DB::table('orders')
            ->join('customers','orders.customer_id','=','customers.id')
            ->where('order_status','Pending')
            ->select('orders.*','customers.name')
            ->orderBy('orders.id','desc')
            ->paginate(12);
        //->join('customers','orders.cuntomer_id','=','customer.id')
        return view('admin.order.pending-order',[
            'orders' => $orders
        ]);
    }

    public function orderDetails($id) {
        // return $id;
        $order    = Order::find($id);
        $customer = Customer::find($order->customer_id);
        $shipping = ShippingInfo::find($order->shipping_id);
        $details  = DB::table('order_details')->where('order_id',$id)->get();

        return view('admin.order.pending-order-details',[
            'order'    => $order,
            'customer' => $customer,
            'shipping' => $shipping,
            'details'  => $details
        ]);
    }

    public function viewInvoice($id) {
        $logos    = DB::table('logos')->where('status',1)->get();
        $order    = Order::find($id);
        $customer = Customer::find($order->customer_id);
        $shipping = ShippingInfo::find($order->shipping_id);
        $details  = DB::table('order_details')->where('order_id',$id)->get();

        return view('admin.order.view-invoice',[
            'logos'    => $logos,
            'order'    => $order,
            'customer' => $customer,
            'shipping' => $shipping,
            'details'  => $details
        ]);
    }

    public function pdfInvoice($id) {
        $logos    = DB::table('logos')->where('status',1)->get();
        $order    = Order::find($id);
        $customer = Customer::find($order->customer_id);
        $shipping = ShippingInfo::find($order->shipping_id);
        $details  = DB::table('order_details')->where('order_id',$id)->get();


        $pdf = PDF::loadView('admin.order.pdf',[
            'logos'    => $logos,
            'order'    => $order,
            'customer' => $customer,
            'shipping' => $shipping,
            'details'  => $details
        ]);

        return $pdf->stream('invoice.pdf');
    }

    public function orderConfirm($id) {
        //return $id;
        $confirmProducts =  DB::table('order_details')->where('order_id',$id)->get();
        foreach ($confirmProducts as $confirmProduct ) {
            $model   = $confirmProduct->product_model;
            $color   = $confirmProduct->product_color;
            $size    = $confirmProduct->product_size;
            $qty     = $confirmProduct->product_quantity;
            $product = DB::table('products')->where('code_no',$model)->where('product_color',$color)->where('product_size',$size)->get();
            foreach ($product as $product ) {
                $previousQty = $product->product_quantity;
                $updateQty   = $previousQty-$qty;
                DB::table('products')->where('id', $product->id)->update(['product_quantity' => $updateQty]);
            }
        }

        DB::table('orders')->where('id', $id)->update(['order_status' => 'Successful']);
        return redirect('/my-ecommerce-administration/order/pending-order')->with('message','Order Update Successfully');
    }

    public function successfulDelivery() {
        $orders = DB::table('orders')
            ->join('customers','orders.customer_id','=','customers.id')
            ->where('order_status','Successful')
            ->select('orders.*','customers.name')
            ->orderBy('orders.id','desc')
            ->paginate(12);
        //->join('customers','orders.cuntomer_id','=','customer.id')
        return view('admin.order.successful-deliver',[
            'orders' => $orders
        ]);
    }

    public function orderCancel($id) {
        $orderById = Order::find($id);
        Order::find($id)->delete();
        DB::table('order_details')->where('order_id',$orderById->id)->delete();
        ShippingInfo::find($orderById->shipping_id);
        return redirect('/my-ecommerce-administration/order/pending-order')->with('message','Order Cancel Successfully');
    }

    public function deliverDetails($id) {
        $order    = Order::find($id);
        $customer = Customer::find($order->customer_id);
        $shipping = ShippingInfo::find($order->shipping_id);
        $details  = DB::table('order_details')->where('order_id',$id)->get();

        return view('admin.order.deliver-order-details',[
            'order'    => $order,
            'customer' => $customer,
            'shipping' => $shipping,
            'details'  => $details
        ]);
    }

    public function deleteDeleveryInfo($id) {
        $orderById = Order::find($id);
        Order::find($id)->delete();
        DB::table('order_details')->where('order_id',$orderById->id)->delete();
        ShippingInfo::find($orderById->shipping_id);
        return redirect('/my-ecommerce-administration/order/successful-delivery')->with('message','Delivery Info Remove Successfully');

    }
}
