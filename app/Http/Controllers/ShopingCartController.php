<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Product;
use App\Brand;
use App\SubCategory;
use Illuminate\Support\Facades\DB;

class ShopingCartController extends Controller
{
    public function addToCart(Request $request) {
        //return $request->all();
        $color           = $request->color;
        $productById     = Product::find($request->product_id);
        $brandById       = DB::table('brands')->where('id',$productById->brand_id)->select('brand_name')->first();
        $subCategoryById = SubCategory::find($productById->sub_category_id);

        Cart::add([
            'id'      => $request->product_id,
            'name'    => $subCategoryById->sub_category_name,
            'qty'     => $request->qty,
            'price'   => $productById->product_prize,
            'options' => [
                'color'            => $color,
                'image'            => $productById->image,
                'brand_name'       => $brandById->brand_name,
                'product_size'     => $request->size,
                'product_quantity' => $productById->product_quantity,
                'product_model'    => $request->model
            ]
        ]);

        return redirect('/show-cart');
   }


   public function showCart() {
        //Cart::destroy();
       $products = Cart::content();
         return view('front.shoping.shoping-cart-table',[
             'products' => $products
         ]);
   }


   public function updateCartProductQuantity(Request $request) {
        //return $request->id;
        Cart::update($request->rowId,$request->qty);
        return redirect('/show-cart');

   }

   public function deleteCartProduct ($id ) {
       Cart::remove($id);
       return redirect('/show-cart');
   }
}
