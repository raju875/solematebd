<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Product;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
use App\SubImage;
use Illuminate\Support\Facades\Input;

class ProductController extends Controller
{
    public function productForm() {
        $countCategory = DB::table('categories')->where('status',1)->count();
        if ($countCategory !=0 ) {
            $countSubCategory = DB::table('sub_categories')->where('status',1)->count();
            if ($countSubCategory != 0 ) {
                $countBrand = DB::table('brands')->where('status',1)->count();
                if ($countBrand != 0 ) {
                    $categories = DB::table('categories')->where('status',1)->pluck("category_name","id");
                    return view('admin.product.product-form',compact('$categories'));
                    //return view('admin.product.product-form');
                }else {
                    return redirect('/my-ecommerce-administration/error/plain-error')->with('alert','There is no published brand !!! Check it and try again.');
                }
            }else {
                return redirect('/my-ecommerce-administration/error/plain-error')->with('alert','There is no sub published category !!! Check it and try again.');
            }
        } else {
            return redirect('/my-ecommerce-administration/error/plain-error')->with('alert','There is no published category !!! Check it and try again.');
        }
    }

    public function dropdownDependency($catId ) {
        $subCategories = DB::table("sub_categories")->where("category_id",$catId)->get();
//        return json_encode($subCategories);
         //return $subCategories;
        foreach ($subCategories as $subCat ) {
            echo "<option value=$subCat->id>$subCat->sub_category_name </option>";
        }

    }

//    public function dropdownDependency() {
//        $category_id   = Input::get('category_id');
//        $subCategories = DB::table('sub_categories')->where('category_id',$category_id)->get();
//        return response()->json($subCategories);
//    }

    public function addNewProduct(Request $request ) {
        //return $request->all();
        $this->validate($request, [
            'category_id'      => 'required',
            'sub_category_id'  => 'required',
            'brand_id'         => 'required',
            'image'            => 'required',
            'sub_image'        => 'required',
            'code_no'          => 'required',
            'product_size'     => 'required',
            'product_quantity' => 'required',
            'product_prize'    => 'required',
            'product_color'    => 'required',
            'description'      => 'required',
            'status'           => 'required'
        ]);
        $count = DB::table('products')->count();
        if ($count!=0 ) {
            $checks = DB::table('products')->get();
            foreach ($checks as $check ) {
                if ($check->code_no==$request->code_no && $check->product_color==$request->product_color ) {
                    return redirect('/my-ecommerce-administration/product/product-form')->with('alert', 'Product code and color are conflict');
                }
            }
        }
        $varifySubCategories = DB::table('sub_categories')->where('category_id',$request->category_id)->get();
        foreach ($varifySubCategories as $varifySubCategory ) {
            if ($varifySubCategory->category_id == $request->category_id ) {
                $image = $request->file('image');
                $imageName = $image->getClientOriginalName();
                $directory = 'admin/product-images/';
                $imageUrl  = $directory . $imageName;
                Image::make($image)->save($imageUrl);

                $sizeGuide          = $request->file('size_guide');
                $sizeGuideName      = $sizeGuide->getClientOriginalName();
                $sizeGuideDirectory = 'admin/product-size-guide/';
                $sizeGuideUrl       = $sizeGuideDirectory . $sizeGuideName;
                Image::make($sizeGuide)->save($sizeGuideUrl);

                $product = new Product();

                $product->category_id      = $request->category_id;
                $product->sub_category_id  = $request->sub_category_id;
                $product->brand_id         = $request->brand_id;
                $product->image            = $imageUrl;
                $product->code_no          = $request->code_no;
                $product->product_size     = $request->product_size;
                $product->product_quantity = $request->product_quantity;
                $product->product_prize    = $request->product_prize;
                $product->product_color    = $request->product_color;
                $product->description      = $request->description;
                $product->size_guide       = $sizeGuideUrl;
                $product->status           = $request->status;

                $product->save();
                $productId = $product->id;

                $productSubImages = $request->file('sub_image');
                foreach ($productSubImages as $productSubImage ) {
                    $SubImageName = $productSubImage->getClientOriginalName();
                    $subImageDirectory = 'admin/product-sub-images/';
                    $subImageUrl = $subImageDirectory . $SubImageName;
                    Image::make($productSubImage)->save($subImageUrl);

                    $subImage = new SubImage();
                    $subImage->product_id = $productId;
                    $subImage->sub_image = $subImageUrl;
                    $subImage->save();

                }
                return redirect('/my-ecommerce-administration/product/product-form')->with('message', 'Product create successfully');
            }

        }
            return redirect('/my-ecommerce-administration/product/product-form')->with('alert', 'Wrong sub category select !!!');
    }

    public function productTable() {
        $products   = DB::table('products')
            ->join('sub_categories','products.sub_category_id','=','sub_categories.id')
            ->join('brands','products.brand_id','=','brands.id')
            ->orderBy('products.id','desc')
            ->select('products.*','sub_categories.sub_category_name','brands.brand_name')->get();
        $categories = DB::table('categories')
            ->join('sub_categories','categories.id','=','sub_categories.category_id')
            ->select('categories.category_name','sub_categories.id')->get();
        $subImages  = DB::table('sub_images')->orderBy('id','desc')->get();
        return view('admin.product.product-table',[
            'products'    => $products,
            'categories'  => $categories,
            'subImages'   => $subImages
        ]);
    }

    public function unpublishedProduct($id){
        DB::table('products')->where('id', $id)->update(['status' => 0]);
        return redirect('/my-ecommerce-administration/product/product-table')->with('message', 'Unpublished product info successfully');
    }

    public function publishedProduct($id){
        DB::table('products')->where('id', $id)->update(['status' => 1]);

        return redirect('/my-ecommerce-administration/product/product-table')->with('message', 'Published product info successfully');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editableProductForm($id){
        $productById     = Product::find($id);
        $categories      = DB::table('categories')->where('status',1)->get();
        $subCategories   = DB::table('sub_categories')->where('status',1)->get();
        $brands          = DB::table('brands')->where('status',1)->get();
        $subImages       = DB::table('sub_images')->where('sub_images.product_id',$id)->get();
        return view('admin.product.editable-product-form', [
            'productById'      => $productById,
            'categories'       => $categories,
            'subCategories'    => $subCategories,
            'brands'           => $brands,
            'subImages'        => $subImages
        ]);
    }

    public function updateProduct(Request $request ){
        //return $request->all();

        switch ($request->input('btn')) {
            case 'copy':
                $product = new Product();
                $productById = Product::find($request->product_id);

                $product->category_id      = $request->category_id;
                $product->sub_category_id  = $productById->sub_category_id;
                $product->brand_id         = $productById->brand_id;
                $product->image            = $productById->image;
                $product->code_no          = $productById->code_no;
                $product->product_size     = $productById->product_size;
                $product->product_quantity = $productById->product_quantity;
                $product->product_prize    = $productById->product_prize;
                $product->product_color    = $productById->product_color;
                $product->description      = $productById->description;
                $product->size_guide       = $productById->size_guide;
                $product->status           = $productById->status;
                $product->save();
                $subImages = SubImage::where('product_id',$request->product_id)->get();
                foreach ($subImages as $image ) {
                    $subImage = new SubImage();
                    $subImage->product_id = $product->id;
                    $subImage->sub_image  = $image->sub_image;
                    $subImage->save();
                }
                return redirect('/my-ecommerce-administration/product/product-table')->with('message', 'Copy product info successfully');

                break;

            case 'update':
                $this->validate($request, [
                    'category_id'      => 'required',
                    'sub_category_id'  => 'required',
                    'brand_id'         => 'required',
                    'code_no'          => 'required',
                    'product_size'     => 'required',
                    'product_quantity' => 'required',
                    'product_prize'    => 'required',
                    'product_color'    => 'required',
                    'description'      => 'required',
                    'status'           => 'required',
                ]);
                if ($request->image == null && $request->sub_image == null && $request->size_guide == null) {
                    $productById = Product::find($request->product_id);

                    $productById->category_id      = $request->category_id;
                    $productById->sub_category_id  = $request->sub_category_id;
                    $productById->brand_id         = $request->brand_id;
                    $productById->code_no          = $request->code_no;
                    $productById->product_size     = $request->product_size;
                    $productById->product_quantity = $request->product_quantity;
                    $productById->product_prize    = $request->product_prize;
                    $productById->product_color    = $request->product_color;
                    $productById->description      = $request->description;
                    $productById->status           = $request->status;

                    $productById->save();
                    return redirect('/my-ecommerce-administration/product/product-table')->with('message', 'Update product info successfully');
                }

                if ($request->image != null && $request->sub_image == null && $request->size_guide == null) {
                    $productImage = $request->file('image');
                    $imageName = $productImage->getClientOriginalName();
                    $directory = 'admin/product-images/';
                    $url = $directory . $imageName;
                    Image::make($productImage)->save($url);

                    $productById = Product::find($request->product_id);

                    $productById->category_id      = $request->category_id;
                    $productById->sub_category_id  = $request->sub_category_id;
                    $productById->brand_id         = $request->brand_id;
                    $productById->image            = $url;
                    $productById->code_no          = $request->code_no;
                    $productById->product_size     = $request->product_size;
                    $productById->product_quantity = $request->product_quantity;
                    $productById->product_prize    = $request->product_prize;
                    $productById->product_color    = $request->product_color;
                    $productById->description      = $request->description;
                    $productById->status           = $request->status;

                    $productById->save();
                    return redirect('/my-ecommerce-administration/product/product-table')->with('message', 'Update product info successfully');
                }

                if ($request->image == null && $request->sub_image != null && $request->size_guide == null ) {

                    DB::table('sub_images')->where('product_id', $request->product_id)->delete();
                    $productById = Product::find($request->product_id);

                    $productById->category_id      = $request->category_id;
                    $productById->sub_category_id  = $request->sub_category_id;
                    $productById->brand_id         = $request->brand_id;
                    $productById->code_no          = $request->code_no;
                    $productById->product_size     = $request->product_size;
                    $productById->product_quantity = $request->product_quantity;
                    $productById->product_prize    = $request->product_prize;
                    $productById->product_color    = $request->product_color;
                    $productById->description      = $request->description;
                    $productById->status           = $request->status;

                    $productById->save();

                    $productSubImages = $request->file('sub_image');
                    foreach ($productSubImages as $productSubImage) {
                        $SubImageName = $productSubImage->getClientOriginalName();
                        $subImageDirectory = 'admin/product-sub-images/';
                        $subImageUrl = $subImageDirectory . $SubImageName;
                        Image::make($productSubImage)->save($subImageUrl);

                        $subImage = new SubImage();

                        $subImage->product_id = $request->product_id;
                        $subImage->sub_image  = $subImageUrl;
                        $subImage->save();
                    }
                    return redirect('/my-ecommerce-administration/product/product-table')->with('message', 'Update product info successfully');
                }

                if ($request->image == null && $request->sub_image == null && $request->size_guide != null ) {

                    DB::table('sub_images')->where('product_id', $request->product_id)->delete();
                    $productById = Product::find($request->product_id);

                    $sizeGuide          = $request->file('size_guide');
                    $sizeGuideName      = $sizeGuide->getClientOriginalName();
                    $sizeGuideDirectory = 'admin/product-size-guide/';
                    $sizeGuideUrl       = $sizeGuideDirectory . $sizeGuideName;
                    Image::make($sizeGuide)->save($sizeGuideUrl);

                    $productById->category_id      = $request->category_id;
                    $productById->sub_category_id  = $request->sub_category_id;
                    $productById->brand_id         = $request->brand_id;
                    $productById->code_no          = $request->code_no;
                    $productById->product_size     = $request->product_size;
                    $productById->product_quantity = $request->product_quantity;
                    $productById->product_prize    = $request->product_prize;
                    $productById->product_color    = $request->product_color;
                    $productById->description      = $request->description;
                    $productById->description      = $sizeGuideUrl;
                    $productById->status           = $request->status;

                    $productById->save();

                    return redirect('/my-ecommerce-administration/product/product-table')->with('message', 'Update product info successfully');
                }

                if ($request->image != null && $request->sub_image != null && $request->size_guide == null ) {

                    DB::table('sub_images')->where('product_id', $request->product_id)->delete();
                    $productById = Product::find($request->product_id);

                    $image = $request->file('image');
                    $imageName = $image->getClientOriginalName();
                    $directory = 'admin/product-images/';
                    $imageUrl  = $directory.$imageName;
                    Image::make($image)->save($imageUrl);

                    $productById->category_id      = $request->category_id;
                    $productById->sub_category_id  = $request->sub_category_id;
                    $productById->brand_id         = $request->brand_id;
                    $productById->image            = $imageUrl;
                    $productById->code_no          = $request->code_no;
                    $productById->product_size     = $request->product_size;
                    $productById->product_quantity = $request->product_quantity;
                    $productById->product_prize    = $request->product_prize;
                    $productById->product_color    = $request->product_color;
                    $productById->description      = $request->description;
                    $productById->status           = $request->status;

                    $productById->save();

                    $productSubImages = $request->file('sub_image');
                    foreach ($productSubImages as $productSubImage) {
                        $SubImageName = $productSubImage->getClientOriginalName();
                        $subImageDirectory = 'admin/product-sub-images/';
                        $subImageUrl = $subImageDirectory . $SubImageName;
                        Image::make($productSubImage)->save($subImageUrl);

                        $subImage = new SubImage();

                        $subImage->product_id = $request->product_id;
                        $subImage->sub_image  = $subImageUrl;
                        $subImage->save();
                    }
                    return redirect('/my-ecommerce-administration/product/product-table')->with('message', 'Update product info successfully');
                }

                if ($request->image != null && $request->sub_image == null && $request->size_guide != null ) {
                    $productById = Product::find($request->product_id);

                    $image = $request->file('image');
                    $imageName = $image->getClientOriginalName();
                    $directory = 'admin/product-images/';
                    $imageUrl  = $directory . $imageName;
                    Image::make($image)->save($imageUrl);

                    $sizeGuide          = $request->file('size_guide');
                    $sizeGuideName      = $sizeGuide->getClientOriginalName();
                    $sizeGuideDirectory = 'admin/product-size-guide/';
                    $sizeGuideUrl       = $sizeGuideDirectory . $sizeGuideName;
                    Image::make($sizeGuide)->save($sizeGuideUrl);

                    $productById->category_id      = $request->category_id;
                    $productById->sub_category_id  = $request->sub_category_id;
                    $productById->brand_id         = $request->brand_id;
                    $productById->image            = $imageUrl;
                    $productById->code_no          = $request->code_no;
                    $productById->product_size     = $request->product_size;
                    $productById->product_quantity = $request->product_quantity;
                    $productById->product_prize    = $request->product_prize;
                    $productById->product_color    = $request->product_color;
                    $productById->description      = $request->description;
                    $productById->size_guide       = $sizeGuideUrl;
                    $productById->status           = $request->status;

                    $productById->save();
                    return redirect('/my-ecommerce-administration/product/product-table')->with('message', 'Update product info successfully');
                }

                if ($request->image == null && $request->sub_image != null && $request->size_guide != null ) {

                    DB::table('sub_images')->where('product_id', $request->product_id)->delete();
                    $productById = Product::find($request->product_id);

                    $sizeGuide          = $request->file('size_guide');
                    $sizeGuideName      = $sizeGuide->getClientOriginalName();
                    $sizeGuideDirectory = 'admin/product-size-guide/';
                    $sizeGuideUrl       = $sizeGuideDirectory . $sizeGuideName;
                    Image::make($sizeGuide)->save($sizeGuideUrl);

                    $productById->category_id      = $request->category_id;
                    $productById->sub_category_id  = $request->sub_category_id;
                    $productById->brand_id         = $request->brand_id;
                    $productById->code_no          = $request->code_no;
                    $productById->product_size     = $request->product_size;
                    $productById->product_quantity = $request->product_quantity;
                    $productById->product_prize    = $request->product_prize;
                    $productById->product_color    = $request->product_color;
                    $productById->description      = $request->description;
                    $productById->size_guide       = $sizeGuideUrl;
                    $productById->status           = $request->status;

                    $productById->save();

                    $productSubImages = $request->file('sub_image');
                    foreach ($productSubImages as $productSubImage) {
                        $SubImageName = $productSubImage->getClientOriginalName();
                        $subImageDirectory = 'admin/product-sub-images/';
                        $subImageUrl = $subImageDirectory . $SubImageName;
                        Image::make($productSubImage)->save($subImageUrl);

                        $subImage = new SubImage();

                        $subImage->product_id = $request->product_id;
                        $subImage->sub_image  = $subImageUrl;
                        $subImage->save();
                    }
                    return redirect('/my-ecommerce-administration/product/product-table')->with('message', 'Update product info successfully');
                }

                if ($request->image != null && $request->sub_image != null && $request->size_guide != null ) {
                    DB::table('sub_images')->where('product_id', $request->product_id)->delete();
                    $productById = Product::find($request->product_id);

                    $image = $request->file('image');
                    $imageName = $image->getClientOriginalName();
                    $directory = 'admin/product-images/';
                    $imageUrl  = $directory . $imageName;
                    Image::make($image)->save($imageUrl);

                    $sizeGuide          = $request->file('size_guide');
                    $sizeGuideName      = $sizeGuide->getClientOriginalName();
                    $sizeGuideDirectory = 'admin/product-size-guide/';
                    $sizeGuideUrl       = $sizeGuideDirectory . $sizeGuideName;
                    Image::make($sizeGuide)->save($sizeGuideUrl);

                    $productById->category_id      = $request->category_id;
                    $productById->sub_category_id  = $request->sub_category_id;
                    $productById->brand_id         = $request->brand_id;
                    $productById->image            = $imageUrl;
                    $productById->code_no          = $request->code_no;
                    $productById->product_size     = $request->product_size;
                    $productById->product_quantity = $request->product_quantity;
                    $productById->product_prize    = $request->product_prize;
                    $productById->product_color    = $request->product_color;
                    $productById->description      = $request->description;
                    $productById->size_guide       = $sizeGuideUrl;
                    $productById->status           = $request->status;

                    $productById->save();
                    $productId = $productById->id;

                    $productSubImages = $request->file('sub_image');
                    foreach ($productSubImages as $productSubImage ) {
                        $SubImageName = $productSubImage->getClientOriginalName();
                        $subImageDirectory = 'admin/product-sub-images/';
                        $subImageUrl = $subImageDirectory . $SubImageName;
                        Image::make($productSubImage)->save($subImageUrl);

                        $subImage = new SubImage();
                        $subImage->product_id = $productId;
                        $subImage->sub_image = $subImageUrl;
                        $subImage->save();

                    }
                    return redirect('/my-ecommerce-administration/product/product-table')->with('message', 'Update product info successfully');

                }
                break;
        }
    }

    public function deleteProductSubImage($id ) {
       $subImageById = SubImage::find($id);
       $count = DB::table('sub_images')->where('product_id',$subImageById->product_id)->count();
       if ($count==1 ) {
           return redirect('/my-ecommerce-administration/product/product-table')->with('alert', 'Number of sub images should be 1');
        }

        SubImage::find($id)->delete();
        return redirect('/my-ecommerce-administration/product/product-table')->with('message', 'Delete sub image successfully');
    }


    public function deleteProductInfo($id ) {
        Product::find($id)->delete();
        DB::table('sub_images')->where('product_id',$id)->delete();
        return redirect('/my-ecommerce-administration/product/product-table')->with('message', 'Delete product info successfully');
    }

}
