<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ErrorController extends Controller
{
    public function plainError() {
        return view('admin.error.plain-error');
    }
}
