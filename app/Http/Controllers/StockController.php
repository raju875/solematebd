<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockController extends Controller
{
    public function stockReport() {
        $categories    = DB::table('categories')->where('status',1)->get();
        $subCategories = DB::table('sub_categories')->where('status',1)->get();
        $products      = DB::table('products')->where('status',1)->get();
        return view('admin.stock.stock-report',[
            'categories'    => $categories,
            'subCategories' => $subCategories,
            'products'      => $products
        ]);
    }

    public function updateStock(Request $request) {
       // return $request->all();
        DB::table('products')->where('id',$request->product_id)->update(['product_quantity' => $request->product_quantity]);
        return redirect('/my-ecommerce-administration/stock/stock-report')->with('message','Update stock info successfully');
    }


    function action(Request $request)
    {
        if($request->ajax())
        {
            $output = '';
            $query = $request->get('query');
            if($query != '')
            {
                $data = DB::table('products')
                    ->join('sub_categories','products.sub_category_id','=','sub_categories.id')
                    ->join('categories','sub_catogories.category_id','=','categories.id')
                    ->where('products.code_no', 'like', '%'.$query.'%')
                    ->orWhere('products.color', 'like', '%'.$query.'%')
                    ->orWhere('products.size', 'like', '%'.$query.'%')
                    ->orWhere('sub_categories.category_name', 'like', '%'.$query.'%')
                    ->orWhere('categories.category_name', 'like', '%'.$query.'%')
                    ->orderBy('products.id', 'desc')
                    ->get();

            }

            $total_row = $data->count();
            if($total_row > 0)
            {
                foreach($data as $row)
                {
                    $output .= '
        <tr>
         <td>'.$row->CustomerName.'</td>
         <td>'.$row->Address.'</td>
         <td>'.$row->City.'</td>
         <td>'.$row->PostalCode.'</td>
         <td>'.$row->Country.'</td>
        </tr>
        ';
                }
            }
            else
            {
                $output = '
       <tr>
        <td align="center" colspan="5">No Data Found</td>
       </tr>
       ';
            }
            $data = array(
                'table_data'  => $output,
                'total_data'  => $total_row
            );

            echo json_encode($data);
        }
    }
}
