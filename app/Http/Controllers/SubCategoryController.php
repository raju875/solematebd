<?php

namespace App\Http\Controllers;

use App\SubCategory;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;

class SubCategoryController extends Controller
{
    public function subCategoryForm() {
        $categories = DB::table('categories')->where('status',1)->get();
        return view('admin.sub-category.sub-category-form',[
            'categories' => $categories
        ]);
    }

    public function addNewSubCategory(Request $request) {
        //return $request->all();
        $this->validate($request, [
            'category_id'       => 'required',
            'sub_category_name' => 'required',
            'image'             => 'required',
            'status'            => 'required'
        ]);

        $image      = $request->file('image');
        $imageName  = $image->getClientOriginalName();
        $directory  = 'admin/sub-category-images/';
        $url        = $directory . $imageName;
        Image::make($image)->save($url);

        $sub = new SubCategory();
        $sub->category_id       = $request->category_id;
        $sub->sub_category_name = $request->sub_category_name;
        $sub->description       = $request->description;
        $sub->image             = $url;
        $sub->status            = $request->status;

        $sub->save();

        return redirect('/my-ecommerce-administration/sub-category/sub-category-form')->with('message', 'New sub category added successfully');
    }

    public function manageSubCategory() {
       $subCategories = DB::table('sub_categories')->join('categories','sub_categories.category_id','=','categories.id')->select('categories.category_name','sub_categories.*')->orderBy('sub_categories.id','desc')->get();
       return view('admin.sub-category.sub-category-table',[
           'subCategories' => $subCategories
       ]);
    }
    public function unpublishedSubCategory($id){
        DB::table('sub_categories')->where('id', $id)->update(['status' => 0]);
        return redirect('/my-ecommerce-administration/sub-category/sub-category-table')->with('message', 'Unpublished sub category info successfully');
    }

    public function publishedSubCategory($id)
    {
        DB::table('sub_categories')->where('id', $id)->update(['status' => 1]);
        return redirect('/my-ecommerce-administration/sub-category/sub-category-table')->with('message', 'Published sub category info successfully');
    }

    public function editableSubCategoryForm($id){
        $subCategoryById = SubCategory::find($id);
        $categoryById    = Category::find($subCategoryById->category_id);
        $categories      = DB::table('categories')->where('status','1')->get();
        return view('admin.sub-category.editable-sub-category-form', [
            'subCategoryById' => $subCategoryById,
            'categoryById'    => $categoryById,
            'categories'      => $categories
        ]);
    }

    public function updateSubCategory(Request $request ) {
        // return $request->category_name;
        $this->validate($request, [
            'sub_category_name' => 'required',
            'status'            => 'required'
        ]);
        if ($request->file('image') == null) {
            $subCategory = SubCategory::find($request->id);
            $subCategory->category_id       = $request->category_id;
            $subCategory->sub_category_name = $request->sub_category_name;
            $subCategory->description       = $request->description;
            $subCategory->status            = $request->status;

            $subCategory->save();

            return redirect('/my-ecommerce-administration/sub-category/sub-category-table')->with('message', 'Update sub category info successfully');
        }

        $image      = $request->file('image');
        $imageName  = $image->getClientOriginalName();
        $directory  = 'admin/sub-category-images/';
        $url        = $directory . $imageName;
        Image::make($image)->save($url);

        $sub = SubCategory::find($request->id);
        $sub->sub_category_name = $request->sub_category_name;
        $sub->description       = $request->description;
        $sub->image             = $url;
        $sub->status            = $request->status;

        $sub->save();

        return redirect('/my-ecommerce-administration/sub-category/sub-category-table')->with('message', 'Update sub category info successfully');
    }

    public function deleteSubCategoryInfo($id )
    {
        SubCategory::find($id)->delete();
        return redirect('/my-ecommerce-administration/sub-category/sub-category-table')->with('message', 'Delete sub category info successfully');
    }


}
