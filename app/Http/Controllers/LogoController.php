<?php

namespace App\Http\Controllers;

use App\Logo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;

class LogoController extends Controller
{
    public function logoForm() {
        return view('admin.logo.logo-form');
    }

    public function addNewLogo(Request $request)
    {
        //return $request->all();
        $this->validate($request, [
            'logo'   => 'required',
            'status' => 'required'
        ]);

        $logo      = $request->file('logo');
        $logoName  = $logo->getClientOriginalName();
        $directory = 'admin/logos/';
        $logoUrl   = $directory . $logoName;
        Image::make($logo)->save($logoUrl);

        $logo = new Logo();
        $logo->logo_name   = $request->logo_name;
        $logo->description = $request->description;
        $logo->logo      = $logoUrl;
        $logo->status      = $request->status;

        $logo->save();

        return redirect('/my-ecommerce-administration/logo/logo-form')->with('message', 'Logo added successfully');
    }

    public function logoManage() {
        $logos = DB::table('logos')->orderBy('id','desc')->get();
        return view('admin.logo.logo-manage',[
            'logos' => $logos
        ]);
    }

    public function unpublishedLogo($id) {
        DB::table('logos')->where('id', $id)->update(['status' => 0]);
        return redirect('/my-ecommerce-administration/logo/logo-table')->with('message', 'Unpublished logo info successfully');
    }

    public function publishedLogo($id) {
        DB::table('logos')->where('id', $id)->update(['status' => 1]);
        return redirect('/my-ecommerce-administration/logo/logo-table')->with('message', 'Published logo info successfully');
    }

    public function logoEditableForm($id)
    {
        $logoById = Logo::find($id);
        return view('admin.logo.editable-logo-form', [
            'logoById' => $logoById
        ]);
    }

    public function updateLogo(Request $request)
    {
        // return $request->category_name;

        if ($request->file('logo') == null) {
            $logo = Logo::find($request->id);
            $logo->logo_name   = $request->logo_name;
            $logo->description = $request->description;
            $logo->status      = $request->status;

            $logo->save();

            return redirect('/my-ecommerce-administration/logo/logo-table')->with('message', 'Update logo info successfully');
        }

        $logo      = $request->file('logo');
        $logoName  = $logo->getClientOriginalName();
        $directory = 'admin/logos/';
        $logoUrl = $directory . $logoName;
        Image::make($logo)->save($logoUrl);

        $logo = Logo::find($request->id);
        $logo->logo_name   = $request->logo_name;
        $logo->logo        = $logoUrl;
        $logo->description = $request->description;
        $logo->status      = $request->status;

        $logo->save();

        return redirect('/my-ecommerce-administration/logo/logo-table')->with('message', 'Update logo info successfully');
    }

    public function deleteLogo($id ) {
        Logo::find($id)->delete();
        return redirect('/my-ecommerce-administration/logo/logo-table')->with('message', 'Logo info delete successfully');
    }

}

