<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;

class ProfileController extends Controller
{
    public function changeProfile() {
        return view('admin.profile.profile',array('user' => Auth::user()) );
    }

    public function updateProfile(Request $request) {
        if ($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $fileName = time().'.'.$avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 200)->save(public_path('admin/profileImages/'.$fileName ));

            $user = Auth::user();
            $user->avatar = $fileName;
            $user->save();

            return view('admin.profile.profile',array('user' => Auth::user()) );

        }
    }

}
