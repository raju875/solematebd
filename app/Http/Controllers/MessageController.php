<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{
    public function messageForm() {
        return view('admin.message.message-form');

    }

    public function addNewMessage(Request $request) {
        //return $request->all();
        $this->validate($request, [
            'message' => 'required',
            'status'  => 'required'
        ]);

        $message = new Message();
        $message->message  = $request->message;
        $message->status   = $request->status;

        $message->save();

        return redirect('/my-ecommerce-administration/message/message-form')->with('message', 'Message added successfully');
    }

    public function messageManage() {
        $messages = Message::all();
        return view('admin.message.message-table',[
            'messages' => $messages
        ]);
    }

    public function unpublishedMessage($id) {
        DB::table('messages')->where('id', $id)->update(['status' => 0]);
        return redirect('/my-ecommerce-administration/message/message-table')->with('message', 'Unpublished messages info successfully');
    }

    public function publishedMessage($id) {
        DB::table('messages')->where('id', $id)->update(['status' => 1]);
        return redirect('/my-ecommerce-administration/message/message-table')->with('message', 'Published messages info successfully');
    }

    public function messageEditableForm($id)
    {
        $messageById = Message::find($id);
        return view('admin.message.editable-message-form', [
            'messageById' => $messageById
        ]);
    }

    public function updateMessage(Request $request)
    {
        //return $request->all();
        $message = Message::find($request->id);
        $message->message = $request->message;
        $message->status  = $request->status;

        $message->save();

        return redirect('/my-ecommerce-administration/message/message-table')->with('message', 'Update message info successfully');
        }
    }
