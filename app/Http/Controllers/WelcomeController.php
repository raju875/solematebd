<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Product;
use App\SubCategory;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WelcomeController extends Controller
{
    public function index() {
        return view('front.home.home');
    }

    public function categoryPage($category ) {
        $cats = DB::table('categories')->where('category_name',$category)->get();
        $products =  DB::table('products')->join('sub_categories','products.sub_category_id','=','sub_categories.id')->join('categories','sub_categories.category_id','=','categories.id')->where('categories.category_name',$category) ->where('products.status',1)->where('categories.status',1)->select('products.*')->orderBy('id','desc')->get();
        return view('front.category.category-page',[
            'products'   => $products,
            'cats' => $cats
        ]);
    }

    public function productPage($id ) {
        $productById      = Product::find($id);
        $subImages        = DB::table('sub_images')->where('product_id',$productById->id)->get();
        $subCategory      = SubCategory::find($productById->sub_category_id);
        $category         = Category::find($subCategory->category_id);
        $branName         = Brand::find($productById->brand_id);
        $relatedProducts  = DB::table('products')->where('sub_category_id',$subCategory->id)->where('status',1)->orderBy('id','desc')->take(18)->get();
        return view('front.product.product-page',[
            'productById'         => $productById,
            'subImages'           => $subImages,
            'subCategory'         => $subCategory,
            'category'            => $category,
            'branName'            => $branName,
            'relatedProducts'     => $relatedProducts
        ]);
    }

    public function selectProductSize($model,$size ) {
       $productBySize = DB::table('products')->where('code_no',$model)->where('product_size',$size)->where('status',1)->first();
       $subImages     = DB::table('sub_images')->where('product_id',$productBySize->id)->get();
       $subCategory   = SubCategory::find($productBySize->sub_category_id);
       $category      = Category::find($subCategory->category_id);
       $brandName     = Brand::find($productBySize->brand_id);
       $productSizes  = DB::table('products')->where('code_no',$model)->where('status',1)->select('product_size')->distinct()->get();
       $productColors = DB::table('products')->where('code_no',$model)->where('product_size',$size)->where('status',1)->select('product_color')->distinct()->get();

        return view('front.product.product-page-size',[
           'productBySize'       => $productBySize,
           'subImages'           => $subImages,
           'subCategory'         => $subCategory,
           'category'            => $category,
           'brandName'           => $brandName,
           'productSizes'        => $productSizes,
           'productColors'       => $productColors
       ]);
    }

    public function selectProductColorBySize($model,$size,$color ) {
        $product = DB::table('products')->where('code_no',$model)->where('product_size',$size)->where('product_color',$color)->get();
        foreach ($product as $product ) {
            $productId     = $product->id;
            $subCategoryId = $product->sub_category_id;
            $brandId       = $product->brand_id;
        }
        $productBySize = Product::find($productId);
        $subImages     = DB::table('sub_images')->where('product_id',$productId)->get();
        $subCategory   = SubCategory::find($subCategoryId);
        $category      = Category::find($subCategory->category_id);
        $brandName     = Brand::find($brandId);
        $productSizes  = DB::table('products')->where('code_no',$model)->where('status',1)->select('product_size')->distinct()->get();
        $productColors = DB::table('products')->where('code_no',$model)->where('product_size',$size)->where('status',1)->select('product_color')->distinct()->get();

        return view('front.product.product-page-size',[
            'productBySize'       => $productBySize,
            'subImages'           => $subImages,
            'subCategory'         => $subCategory,
            'category'            => $category,
            'brandName'           => $brandName,
            'productSizes'        => $productSizes,
            'productColors'       => $productColors
        ]);
    }






    public function selectProductColor($productId,$color) {
       $pro = Product::find($productId);
       $selectProducts = DB::table('products')->where('code_no',$pro->code_no)->where('product_color',$color)->get();
       foreach ($selectProducts as $product ) {
           echo $product->image;
       }
    }

    public function newArrivalProducts($color) {
        echo $color;
//       $newArrivals = DB::table('products')->orderBy('id','desc')->take(7)->get();
//       foreach ($newArrivals as $newArrival ) {
//           echo $newArrival->image;
//           echo $newArrival->product_prize;
//       }

    }


}
