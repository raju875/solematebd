<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;

class CategoryController extends Controller
{
    public function categoryForm()
    {
        return view('admin.category.category-form');
    }

    public function addCategory(Request $request)
    {
        //return $request->all();
        $this->validate($request, [
            'category_name' => 'required',
            'image' => 'required',
            'status' => 'required'
        ]);

        $image = $request->file('image');
        $imageName = $image->getClientOriginalName();
        $directory = 'admin/category-images/';
        $imageUrl = $directory . $imageName;
        Image::make($image)->save($imageUrl);

        $category = new Category();
        $category->category_name = $request->category_name;
        $category->description = $request->description;
        $category->image = $imageUrl;
        $category->status = $request->status;

        $category->save();

        return redirect('/my-ecommerce-administration/category/add-category-form')->with('message', 'Category added successfully');
    }

    public function manageCategory()
    {
        $allCategories = DB::table('categories')->orderBy('id', 'desc')->get();
        return view('admin.category.category-manage', [
            'allCategories' => $allCategories
        ]);
    }

    public function unpublishedCategory($id)
    {
        DB::table('categories')->where('id', $id)->update(['status' => 0]);

        return redirect('/my-ecommerce-administration/category/category-table')->with('message', 'Unpublished category info successfully');
    }

    public function publishedCategory($id)
    {
        DB::table('categories')->where('id', $id)->update(['status' => 1]);

        return redirect('/my-ecommerce-administration/category/category-table')->with('message', 'Uublished category info successfully');
    }

    public function editableCategoryForm($id)
    {
        $CategoryById = Category::find($id);
        return view('admin.category.editable-category-form', [
            'CategoryById' => $CategoryById
        ]);
    }

    public function updateCategory(Request $request)
    {
        // return $request->category_name;
        $this->validate($request, [
            'category_name' => 'required'
        ]);
        if ($request->file('image') == null) {
            $category = Category::find($request->id);
            $category->category_name = $request->category_name;
            $category->description = $request->description;
            $category->status = $request->status;

            $category->save();

            return redirect('/my-ecommerce-administration/category/category-table/' . $request->id)->with('message', 'Category infi update successfully');
        }

        $image = $request->file('image');
        $imageName = $image->getClientOriginalName();
        $directory = 'admin/category-images/';
        $imageUrl = $directory . $imageName;
        Image::make($image)->save($imageUrl);

        $category = Category::find($request->id);
        $category->category_name = $request->category_name;
        $category->description = $request->description;
        $category->image = $imageUrl;
        $category->status = $request->status;

        $category->save();

        return redirect('/my-ecommerce-administration/category/category-table/' . $request->id)->with('message', 'Category info update successfully');
    }

    public function deleteCategoryInfo($id)
    {
        Category::find($id)->delete();
        return redirect('/my-ecommerce-administration/category/category-table')->with('message', 'Delete category info successfully');
    }

    public function viewCategoryInfo($id)
    {
        $category = Category::find($id);
        return view('admin.category.view-category', [
            'category' => $category
        ]);
    }
}
